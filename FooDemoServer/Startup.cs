﻿#define BAD_BAD_BAD_ALLOW_DB_ENTITY_ON_API

using FooDemoServer.Controllers.Spike01Controllers.DTOs;
using FooDemoServer.Shared;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;
using Newtonsoft.Json;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;

namespace FooDemoServer
{

    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();

            //AS Per:
            //https://docs.microsoft.com/en-us/azure/active-directory-b2c/active-directory-b2c-devquickstarts-api-dotnet
            //After you've done
            //PM > Install - Package Microsoft.Owin.Security.OAuth - ProjectName TaskService
            //PM > Install - Package Microsoft.Owin.Security.Jwt - ProjectName TaskService
            //PM > Install - Package Microsoft.Owin.Host.SystemWeb - ProjectName TaskService

            //ConfigureAuth(app);

            EnableCors(config);
            EnableStaticFilesForImagesEtc(appBuilder);

            // Observation:
            // Seems to require being done before Odata routes
            // or else EnsureInitialized gets unhappy:
            config.MapHttpAttributeRoutes();
            appBuilder.UseWebApi(config);


            // JSON chokes on most EF models:
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            DevelopODataSchema(config);
            DevelopWebApiRoutes(config);

            config.EnsureInitialized();
        }

        void EnableCors(HttpConfiguration config)
        {
            // Cross Origin Request scripting is required because this service -- hosted at localhost:9000
            // needs to accept requests from the front end site, hosted somewhere else:
            // http://localhost:4200
            var cors = new EnableCorsAttribute("*", "*", "*", "*");
            config.EnableCors(cors);

        }

        void EnableStaticFilesForImagesEtc(IAppBuilder appBuilder)
        {
            //See: https://codeopinion.com/asp-net-self-host-static-file-server/
            appBuilder.UseFileServer("/assets");
        }


            void DevelopWebApiRoutes(HttpConfiguration config)
        {
            //Setup defaults for older WebAPI routing.
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/legacy/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }


        /*
        * Configure the authorization OWIN middleware.
        */
        //public void ConfigureAuth(IAppBuilder app)
        //{
        //// These values are pulled from web.config
        //public static string AadInstance = ConfigurationManager.AppSettings["ida:AadInstance"];
        //public static string Tenant = ConfigurationManager.AppSettings["ida:Tenant"];
        //public static string ClientId = ConfigurationManager.AppSettings["ida:ClientId"];
        //public static string SignUpSignInPolicy = ConfigurationManager.AppSettings["ida:SignUpSignInPolicyId"];
        //public static string DefaultPolicy = SignUpSignInPolicy;
        //    TokenValidationParameters tvps = new TokenValidationParameters
        //    {
        //        // Accept only those tokens where the audience of the token is equal to the client ID of this app
        //        ValidAudience = ClientId,
        //        AuthenticationType = Startup.DefaultPolicy
        //    };

        //    app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
        //    {
        //        // This SecurityTokenProvider fetches the Azure AD B2C metadata & signing keys from the OpenIDConnect metadata endpoint
        //        AccessTokenFormat = new JwtFormat(tvps, new OpenIdConnectCachingSecurityTokenProvider(String.Format(AadInstance, Tenant, DefaultPolicy)))
        //    });
        //}

        // Entities need to be 'described'. This was a design decision in order for
        // OData to manage other sources than just EF. 
        // Since I'm pushing out DTOs this actually works for us.
        // The most important req is to define what is the Key of an entity.
        //
        // At first, this seems to suck. 
        // You think that if you had used DataAnnotations on your EF model
        // instead of FluentAPI definitions, you could have benefited from 
        // reuse here.
        // But that probably would be an incorrect conclusion: a maintainable 
        // app uses DTOs for APIs and never exposes the real db entities over APIs. 
        // So you would *still* have to define the API's relationships here...
        void DevelopODataSchema(HttpConfiguration config)
        {
            //Now define the Odata mapping (why it's not the same as above, dunno):
            //Add this to get odata to work.
            //Can be: EdmModel|ODataModelBuilder|ODataConventionModelBuilder:
            ODataModelBuilder odataModelBuilder = new ODataConventionModelBuilder();

            DefineODataEntitySets(odataModelBuilder);

            // Key tasks when defining OData relationships is ensuring 
            // it knows what is the ID for every entity. Since I use 'Id' instead of the 
            // general convention of 'FooId', I have to define them by hand... bummer.
            DefineODataEntityIndexProperties(odataModelBuilder);

            // The next task is to define the relationship between objects.
            DefineODataEntityRelationships(odataModelBuilder);



            //Make a route
            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: "api",
                model: odataModelBuilder.GetEdmModel());

        }



        void DefineODataEntitySets(ODataModelBuilder odataModelBuilder)
        {
#if BAD_BAD_BAD_ALLOW_DB_ENTITY_ON_API


            //Map the entities to a controller (TIP: name must match controller name).

            odataModelBuilder.EntitySet<Tenant>("tenant");
            odataModelBuilder.EntitySet<User>("user");
            odataModelBuilder.EntitySet<Session>("session");
            odataModelBuilder.EntitySet<SessionOperation>("sessionoperation");


            odataModelBuilder.EntitySet<Foo>("fooitem");

            odataModelBuilder.EntitySet<Body>("body");
            odataModelBuilder.EntitySet<BodyCategory>("bodycategory");
            odataModelBuilder.EntitySet<BodyAlias>("bodyname");
            odataModelBuilder.EntitySet<BodyProperty>("bodyproperty");
            odataModelBuilder.EntitySet<BodyClaim>("bodyclaim");
            odataModelBuilder.EntitySet<BodyChannel>("bodychannel");

            odataModelBuilder.EntitySet<Spike01Item>("spike01item");
            odataModelBuilder.EntitySet<Spike01SubItem>("spike01subitem");
            odataModelBuilder.EntitySet<Spike01SubItemOrderUpdate>("spike01subitemorderupdate");

#endif
            odataModelBuilder.EntitySet<TenantDto>("tenantdto");
            odataModelBuilder.EntitySet<UserDto>("userdto");
            odataModelBuilder.EntitySet<SessionDto>("sessiondto");
            odataModelBuilder.EntitySet<SessionOperationDto>("sessionoperationdto");


            
            //How this works is you define an Entity, and map it to a string, that gets converted into a path.
            //So...say you enter http://localhost:9000/api/bodydtotest/
            //and because you said above that any url that starts with 'api' gets handled 
            //via odata, it then tries to find a controller called BodyDtoTestController. 
            //But even if you have a controller called BodyDtoTestController, it won't route to it,
            //until you've made a dataset named as 'bodydtotest':
            odataModelBuilder.EntitySet<BodyDto>("bodydtotest");
            //You can register a set with two different names. They both will work (as long
            //as you have two controllers: BodyDtoTestController and BodyDto2TestController
            //odataModelBuilder.EntitySet<BodyDto>("bodydtotest2");

            //It also works if you decorate the controller with [ODataRoutePrefix("bodybasic")]
            //as long as you don't forget to *also* decorate methods with [ODataRoute()]
            //as per https://stackoverflow.com/questions/29499378/odata-v4-routing-prefix
            odataModelBuilder.EntitySet<BodyDto>("bodybasic");



            //odataModelBuilder.EntitySet<FooSubItem>("foosubitem");
            odataModelBuilder.EntitySet<FooDto>("foodto");
            odataModelBuilder.EntitySet<FooDto>("fooitemdto");
            //odataModelBuilder.EntitySet<FooSubItemDto>("foo");




            //odataModelBuilder.EntitySet<SearchResponseItemDto>("search");
            odataModelBuilder.EntitySet<SearchResponseItemDto>("search");
            odataModelBuilder.EntitySet<SearchResponseItemDto>("searchresponseitemdto");


            odataModelBuilder.EntitySet<MessageDto>("messagedto");

            odataModelBuilder.EntitySet<NotificationDto>("notification");

            
            odataModelBuilder.EntitySet<BodyDto>("bodydto");
            odataModelBuilder.EntitySet<BodyCategoryDto>("bodycategorydto");
            odataModelBuilder.EntitySet<BodyAliasDto>("bodynamedto");
            odataModelBuilder.EntitySet<BodyPropertyDto>("bodypropertydto");
            odataModelBuilder.EntitySet<BodyClaimDto>("bodyclaimdto");
            //
            odataModelBuilder.EntitySet<BodyChannelDto>("bodychanneldto");
        }


        void DefineODataEntityIndexProperties(ODataModelBuilder odataModelBuilder)
        {
#if BAD_BAD_BAD_ALLOW_DB_ENTITY_ON_API
            //EF ENTITIES (FOR STRAIGHT THROUGH):
            odataModelBuilder.EntityType<User>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<Tenant>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<Session>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<SessionOperation>().HasKey(x => x.Id);

            odataModelBuilder.EntityType<Foo>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<FooSubItem>().HasKey(x => x.Id);
            //Only Turned on in order to find the bug in direct controller...
            odataModelBuilder.EntityType<Body>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyCategory>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyAlias>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyProperty>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyClaim>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyChannel>().HasKey(x => x.Id);

            odataModelBuilder.EntityType<Spike01Item>().HasKey(x => x.Id);;
            odataModelBuilder.EntityType<Spike01SubItem>().HasKey(x => x.Id);
            //Not requiring Odata:
            //odataModelBuilder.EntityType<Spike01SubItemOrderUpdate>()=);

#endif
            //DTOS (WHAT WE SHOULD BE DOING:
            odataModelBuilder.EntityType<UserDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<TenantDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<SessionDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<SessionOperationDto>().HasKey(x => x.Id);

            odataModelBuilder.EntityType<FooDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<FooSubItemDto>().HasKey(x => x.Id);

            odataModelBuilder.EntityType<BodyDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyCategoryDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyAliasDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyPropertyDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyClaimDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<BodyChannelDto>().HasKey(x => x.Id);

        }
        void DefineODataEntityRelationships(ODataModelBuilder odataModelBuilder)
        {
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Category);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Names);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Properties);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Claims);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Channels);


        }

    }
}