﻿using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.DbContextModelBuilder
{


    public static class ProjectDbContextModeBuilderDefineSpike01Item
    {

        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<Spike01Item>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();


            modelBuilder.Entity<Spike01Item>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.IsOrg)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.OrgName)
                .HasColumnOrder(order++)
                .IsOptional();

            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.First)
                .HasColumnOrder(order++)
                .IsOptional();
            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.Middles)
                .HasColumnOrder(order++)
                .IsOptional();
            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.Last)
                .HasColumnOrder(order++)
                .IsOptional();
            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.Description)
                .HasColumnOrder(order++)
                .IsOptional();
            modelBuilder.Entity<Spike01Item>()
                .Property(x => x.Notes)
                .HasColumnOrder(order++)
                .IsOptional();


            modelBuilder.Entity<Spike01Item>()
                .HasMany(x => x.Items)
                .WithRequired() //An order can be without a specific Customer (eg: till), but if does, needs a Customer property.
                .HasForeignKey(x => x.ParentFK);
                    }


    }

}
