﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineSpike01SubItem
    {

        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<Spike01SubItem>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();

            modelBuilder.Entity<Spike01SubItem>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.ParentFK)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.DisplayOrderHint)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.Protocol)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.Title)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.Address)
                .HasColumnOrder(order++)
                .IsOptional();



            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.Street)
                .HasColumnOrder(order++)
                .IsOptional();
            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.City)
                .HasColumnOrder(order++)
                .IsOptional();
            modelBuilder.Entity<Spike01SubItem>()
                .Property(x => x.Country)
                .HasColumnOrder(order++)
                .IsOptional();
        }


    }

}
