﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineSessionOperation
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<SessionOperation>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<SessionOperation>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

                modelBuilder.Entity<SessionOperation>()
                    .HasRequired(x=>x.Session)
                    .WithMany()
                    .HasForeignKey(x => x.SessionFK);

       

            modelBuilder.Entity<SessionOperation>()
                    .Property(x => x.ClientIP)
                    .HasColumnOrder(order++)
                    .HasMaxLength(1024)
                    .IsRequired();

        modelBuilder.Entity<SessionOperation>()
                    .Property(x => x.Key)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<SessionOperation>()
                        .Property(x => x.Notes)
                        .HasColumnOrder(order++)
                        .HasMaxLength(50)
                        .IsRequired();
        }
    }
    

}
