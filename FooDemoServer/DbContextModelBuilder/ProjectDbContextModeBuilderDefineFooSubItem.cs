﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineFooSubItem
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<FooSubItem>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<FooSubItem>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

            modelBuilder.Entity<FooSubItem>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<FooSubItem>()
                    .Property(x => x.RecordState)
                    .HasColumnOrder(order++)
                    .IsRequired();

            modelBuilder.Entity<FooSubItem>()
                .Property(x => x.ParentFK)
                .HasColumnOrder(order++)
                .IsRequired();





        }
    }

}
