﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineSession
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<Session>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<Session>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

            modelBuilder.Entity<Session>()
                    .Property(x => x.Enabled)
                    .HasColumnOrder(order++)
                    .IsRequired();

            modelBuilder.Entity<Session>()
                    .Property(x => x.StartDateTimeUtc)
                    .HasColumnOrder(order++)
                    .IsRequired();

            modelBuilder.Entity<Session>()
                .HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserFK);

            modelBuilder.Entity<Session>()
                .HasRequired(x => x.Tenant)
                .WithMany()
                .HasForeignKey(x => x.TenantFK);


        }
    }


}
