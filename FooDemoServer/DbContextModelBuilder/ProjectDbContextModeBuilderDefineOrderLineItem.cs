﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineOrderLineItem
    {


        public static void Define(DbModelBuilder modelBuilder)
        {

            var order = 1;

            modelBuilder.Entity<OrderLineItem>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<OrderLineItem>()
                .Property(x => x.Id)

                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();


            modelBuilder.Entity<OrderLineItem>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<OrderLineItem>()
                .Property(x => x.RecordState)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<OrderLineItem>()
                .Property(x => x.Text)
                .HasMaxLength(50)
                .HasColumnOrder(order++)
                .IsRequired();



        }
    }

}
