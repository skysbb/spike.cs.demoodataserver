﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineOrder
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<Order>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Order>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();

            modelBuilder.Entity<Order>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .Property(x => x.RecordState)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(x => x.Ref)
                .HasMaxLength(50)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .HasMany(x => x.LineItems)
                .WithRequired() //An lineitem must belong to an Order, but does not need an Order property. 
                .HasForeignKey(x => x.OrderFK);

        }
    }

}
