﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineBodyClaim
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<BodyClaim>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<BodyClaim>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();


            modelBuilder.Entity<BodyClaim>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<BodyClaim>()
                    .Property(x => x.OwnerFK)
                    .HasColumnOrder(order++)
                    .IsRequired();

                modelBuilder.Entity<BodyClaim>()
                    .Property(x => x.Authority)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();
                modelBuilder.Entity<BodyClaim>()
                    .Property(x => x.Key)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();

                modelBuilder.Entity<BodyClaim>()
                    .Property(x => x.Value)
                    .HasColumnOrder(order++)
                    .HasMaxLength(1024)
                    .IsOptional();
                modelBuilder.Entity<BodyClaim>()
                    .Property(x => x.AuthoritySignature)
                    .HasColumnOrder(order++)
                    .HasMaxLength(1024)
                    .IsRequired();


        }
    }

}
