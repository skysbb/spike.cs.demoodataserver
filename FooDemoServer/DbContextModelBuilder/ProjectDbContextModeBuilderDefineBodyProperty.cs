﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineBodyProperty
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<BodyProperty>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<BodyProperty>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();

            modelBuilder.Entity<BodyProperty>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<BodyProperty>()
                .Property(x => x.OwnerFK)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<BodyProperty>()
                    .Property(x => x.Key)
                    .HasColumnOrder(order++)
                .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<BodyProperty>()
                .Property(x => x.Value)
                .HasColumnOrder(order++)
                .HasMaxLength(1024)
                .IsOptional();


        }
    }

}
