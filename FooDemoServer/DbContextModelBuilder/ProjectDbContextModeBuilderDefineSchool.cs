﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineSchool
    {


            public static void Define(DbModelBuilder modelBuilder)
            {
                var order = 1;

                modelBuilder.Entity<School>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<School>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();


            modelBuilder.Entity<School>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<School>()
                .HasRequired<Body>(x => x.Organisation)
                .WithMany()
                .HasForeignKey(x => x.OrganisationFK)
                    .WillCascadeOnDelete(false);


                modelBuilder.Entity<School>()
                    .HasRequired<Body>(x => x.Principal)
                    .WithMany()
                    .HasForeignKey(x => x.PrincipalFK)
                    .WillCascadeOnDelete(false);



        }
    }

}
