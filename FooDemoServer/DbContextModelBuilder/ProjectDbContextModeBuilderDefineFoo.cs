﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{


    public static class ProjectDbContextModeBuilderDefineFoo
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<Foo>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Foo>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();


            modelBuilder.Entity<Foo>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<Foo>()
                .Property(x => x.RecordState)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Foo>()
                .Property(x => x.Text)
                .HasColumnOrder(order++)
                .IsRequired();


            modelBuilder.Entity<Foo>()
                .Property(x => x.Secret)
                .HasColumnOrder(order++)
                .IsRequired();


            // Relationships.
            modelBuilder.Entity<Foo>()
                .HasMany(x => x.Items)
                .WithRequired() //An order can be without a specific Customer (eg: till), but if does, needs a Customer property.
                .HasForeignKey(x => x.ParentFK);


        }
    }

}
