﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineBody
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<Body>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<Body>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

                modelBuilder.Entity<Body>()
                    .HasRequired(x=>x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Body>()
                    .Property(x => x.RecordState)
                    .HasColumnOrder(order++)
                    .IsRequired();

                modelBuilder.Entity<Body>()
                    .Property(x => x.Type)
                    .HasColumnOrder(order++)
                    .IsRequired();
                modelBuilder.Entity<Body>()
                    .HasRequired(x => x.Category)
                    .WithMany()
                    .HasForeignKey(x => x.CategoryFK);
                modelBuilder.Entity<Body>()
                    .Property(x => x.Key)
                    .HasColumnOrder(order++)
                    .HasMaxLength(256)
                    .IsOptional();
                modelBuilder.Entity<Body>()
                    .Property(x => x.Description)
                    .HasColumnOrder(order++)
                    .HasMaxLength(256)
                    .IsOptional();
                modelBuilder.Entity<Body>()
                    .HasMany(x => x.Properties)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);
                modelBuilder.Entity<Body>()
                    .HasMany(x => x.Claims)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);
                modelBuilder.Entity<Body>()
                    .HasMany(x => x.Aliases)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);
                modelBuilder.Entity<Body>()
                    .HasMany(x => x.Channels)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);
                modelBuilder.Entity<Body>()
                    .Property(x => x.Notes)
                    .HasColumnOrder(order++)
                    .HasMaxLength(2048)
                    .IsOptional();

        }
    }
    

}
