﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineCustomer
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<Customer>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Customer>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();


            modelBuilder.Entity<Customer>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<Customer>()
                .Property(x => x.RecordState)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Customer>()
                .Property(x => x.Ref)
                .HasColumnOrder(order++)
                .IsRequired();


            modelBuilder.Entity<Customer>()
               .HasOptional(x => x.Body)
               .WithMany()
               .HasForeignKey(x => x.BodyFK) //Uses its own FK to do it.
               .WillCascadeOnDelete(false) //so that if you delete the Address, the user is not lost.
               ;

            modelBuilder.Entity<Customer>()
                .HasMany(x => x.Orders)
                .WithOptional(x => x.Customer) //An order can be without a specific Customer (eg: till), but if does, needs a Customer property.
                .HasForeignKey(x => x.CustomerFK);



        }
    }

}
