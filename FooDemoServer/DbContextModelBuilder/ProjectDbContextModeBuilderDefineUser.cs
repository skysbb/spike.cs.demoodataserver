﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineUser
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<User>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<User>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

            modelBuilder.Entity<User>()
                    .Property(x => x.Enabled)
                    .HasColumnOrder(order++)
                    .IsRequired();

            modelBuilder.Entity<User>()
                    .Property(x => x.DisplayName)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<User>()
                    .HasMany(x => x.Properties)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);

            modelBuilder.Entity<User>()
                    .HasMany(x => x.Claims)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);

        }
    }


}
