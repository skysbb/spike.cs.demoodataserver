﻿using FooDemoServer.DbContextModelBuilder;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Application
{
    public class ProjectDbContextModelBuilder
    {

        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("module1");


            ProjectDbContextModeBuilderDefineUserProperty.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineUserClaim.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineUser.Define(modelBuilder);


            ProjectDbContextModeBuilderDefineTenantProperty.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineTenantClaim.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineTenant.Define(modelBuilder);

            ProjectDbContextModeBuilderDefineSession.Define(modelBuilder);


            ProjectDbContextModeBuilderDefineCustomer.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineOrder.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineOrderLineItem.Define(modelBuilder);

            ProjectDbContextModeBuilderDefineBodyCategory.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineBodyChannel.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineBodyProperty.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineBodyClaim.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineBodyAlias.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineBody.Define(modelBuilder);

            ProjectDbContextModeBuilderDefineSchool.Define(modelBuilder);

            ProjectDbContextModeBuilderDefineFoo.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineFooSubItem.Define(modelBuilder);

            ProjectDbContextModeBuilderDefineSpike01Item.Define(modelBuilder);
            ProjectDbContextModeBuilderDefineSpike01SubItem.Define(modelBuilder);

        }











    }
}
