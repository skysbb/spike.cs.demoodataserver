﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineBodyCategory
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<BodyCategory>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<BodyCategory>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

            modelBuilder.Entity<BodyCategory>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<BodyCategory>()
                    .Property(x => x.RecordState)
                    .HasColumnOrder(order++)
                    .IsRequired();
                modelBuilder.Entity<BodyCategory>()
                    .Property(x => x.Text)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();


        }
    }

}
