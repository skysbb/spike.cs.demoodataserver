﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{
    public static class ProjectDbContextModeBuilderDefineBodyChannel
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            modelBuilder.Entity<BodyChannel>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<BodyChannel>()
                .Property(x => x.Id)
                .HasColumnOrder(order++)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();


            modelBuilder.Entity<BodyChannel>()
                    .HasRequired(x => x.Tenant)
                    .WithMany()
                    .HasForeignKey(x => x.TenantFK)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<BodyChannel>()
                .Property(x => x.OwnerFK)
                .HasColumnOrder(order++)
                .IsRequired();
            modelBuilder.Entity<BodyChannel>()
                .Property(x => x.DisplayOrderHint)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<BodyChannel>()
                .Property(x => x.Protocol)
                .HasColumnOrder(order++)
                //.HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.Title)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
                .HasMaxLength(50)
               .IsRequired();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.Address)
               .HasColumnOrder(order++)
               .HasMaxLength(256)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressStreetAndNumber)
               .HasColumnOrder(order++)
               .HasMaxLength(256)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressSuburb)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressCity)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressRegion)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressState)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressPostalCode)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressCountry)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();
            modelBuilder.Entity<BodyChannel>()
               .Property(x => x.AddressInstructions)
               .HasColumnOrder(order++)
               .HasMaxLength(100)
               .IsOptional();

        }
    }

}
