﻿using FooDemoServer.Shared.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace FooDemoServer.DbContextModelBuilder
{

    public static class ProjectDbContextModeBuilderDefineTenant
    {


        public static void Define(DbModelBuilder modelBuilder)
        {
                var order = 1;

                modelBuilder.Entity<Tenant>()
                    .HasKey(x => x.Id);

                modelBuilder.Entity<Tenant>()
                    .Property(x => x.Id)
                    .HasColumnOrder(order++)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                    .IsRequired();

            modelBuilder.Entity<Tenant>()
                    .Property(x => x.Enabled)
                    .HasColumnOrder(order++)
                    .IsRequired();

            modelBuilder.Entity<Tenant>()
                    .Property(x => x.Key)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<Tenant>()
                    .Property(x => x.DisplayName)
                    .HasColumnOrder(order++)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<Tenant>()
                    .HasMany(x => x.Properties)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);

            modelBuilder.Entity<Tenant>()
                    .HasMany(x => x.Claims)
                    .WithOptional()
                    .HasForeignKey(x => x.OwnerFK);

        }
    }


}
