﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{
    public class Map_BodyProperty_BodyPropertyDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<BodyProperty, BodyPropertyDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                      .ForMember(t => t.BodyFK, opt => opt.MapFrom(s => s.OwnerFK))
                      .ForMember(t => t.RecordState, opt => opt.MapFrom(s => s.RecordState))
                      .ForMember(t => t.Key, opt => opt.MapFrom(s => s.Key))
                      .ForMember(t => t.Value, opt => opt.MapFrom(s => s.Value))
                      ;

        }
    }



}
