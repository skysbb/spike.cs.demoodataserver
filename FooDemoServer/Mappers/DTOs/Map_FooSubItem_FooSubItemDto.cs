﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{

    public partial class AutoMapperConfiguration
    {
    }

        public class Map_FooSubItem_FooSubItemDto
        {
            public static void Initialize(IMapperConfigurationExpression config)
            {
                config.CreateMap<FooSubItem, FooSubItemDto>()
                          .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                          .ForMember(t => t.RecordState, opt => opt.MapFrom(s => s.RecordState))
                          .ForMember(t => t.Text, opt => opt.MapFrom(s => s.Text))
                          .ForMember(t => t.ParentFK, opt => opt.MapFrom(s => s.ParentFK))
                          .ForMember(t => t.Parent, opt => opt.Ignore())
                          ;


            }
        }



}
