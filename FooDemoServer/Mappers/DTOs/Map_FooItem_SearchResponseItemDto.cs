﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{
    public class Map_FooItem_SearchResponseItemDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<Foo, SearchResponseItemDto>()
                              .ForMember(t => t.SourceTypeKey, opt => opt.MapFrom(s => "TODO..."))
                              .ForMember(t => t.SourceIdentifier, opt => opt.MapFrom(s => s.Id))
                              .ForMember(t => t.Title, opt => opt.MapFrom(s => s.Text))
                              .ForMember(t => t.Description, opt => opt.MapFrom(s => "Blah...Blah...Blah..."))
                              .ForMember(t => t.ImageUrl, opt => opt.MapFrom(s => s.ImagePath))
                              ;


        }
    }






}
