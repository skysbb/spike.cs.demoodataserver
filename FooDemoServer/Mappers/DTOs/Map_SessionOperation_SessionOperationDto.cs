﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{

    public class Map_SessionOperation_SessionOperationDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<SessionOperation, SessionOperationDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.SessionFK, opt => opt.MapFrom(s => s.SessionFK))
                      .ForMember(t => t.ClientIP, opt => opt.MapFrom(s => s.ClientIP))
                      .ForMember(t => t.Key, opt => opt.MapFrom(s => s.Key))
                      .ForMember(t => t.Notes, opt => opt.MapFrom(s => s.Notes))
                      ;

        }
    }
}







