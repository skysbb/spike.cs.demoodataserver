﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{
    public class Map_Session_SessionDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<Session, SessionDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                      .ForMember(t => t.Enabled, opt => opt.MapFrom(s => s.Enabled))
                      .ForMember(t => t.StartDateTimeUtc, opt => opt.MapFrom(s => s.StartDateTimeUtc))
                      .ForMember(t => t.UserFK, opt => opt.MapFrom(s => s.UserFK))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                      ;

        }
    }



}
