﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{
    public class Map_BodyCategory_BodyCategoryDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<BodyCategory, BodyCategoryDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                      .ForMember(t => t.RecordState, opt => opt.MapFrom(s => s.RecordState))
                      .ForMember(t => t.Text, opt => opt.MapFrom(s => s.Text))
                      ;

        }
    }



}
