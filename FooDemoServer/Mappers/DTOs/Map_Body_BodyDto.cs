﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{

    public class Map_Body_BodyDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<Body, BodyDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                      //.ForMember(t => t.BodyFK, opt => opt.MapFrom(s => s.))
                      .ForMember(t => t.RecordState, opt => opt.MapFrom(s => s.RecordState))
                      .ForMember(t => t.Type, opt => opt.MapFrom(s => s.Type))

                      .ForMember(t => t.CategoryFK, opt => opt.MapFrom(s => s.CategoryFK))
                      .ForMember(t => t.Category, opt => opt.MapFrom(s => s.Category))
                      
                      .ForMember(t => t.Key, opt => opt.MapFrom(s => s.Key))

                      .ForMember(t => t.Prefix, opt => opt.MapFrom(s => s.Prefix))
                      .ForMember(t => t.Name, opt => opt.MapFrom(s => s.Name))
                      .ForMember(t => t.GivenName, opt => opt.MapFrom(s => s.GivenName))
                      .ForMember(t => t.MiddleNames, opt => opt.MapFrom(s => s.MiddleNames))
                      .ForMember(t => t.SurName, opt => opt.MapFrom(s => s.SurName))
                      .ForMember(t => t.Suffix, opt => opt.MapFrom(s => s.Suffix))


                      .ForMember(t => t.Names, opt => opt.MapFrom(s => s.Aliases))
                      .ForMember(t => t.Channels, opt => opt.MapFrom(s => s.Channels))
                      .ForMember(t => t.Properties, opt => opt.MapFrom(s => s.Properties))
                      .ForMember(t => t.Claims, opt => opt.MapFrom(s => s.Claims))
                      ;

        }
    }
}







