﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Mappers.DTOs.V1
{

    public partial class AutoMapperConfiguration
    {
    }
        public class CustomResolver : IValueResolver<Foo, FooDto, string>
        {
            public string Resolve(Foo source, FooDto destination, string memberdestmember, ResolutionContext context)
            {
                return "SECRET REMOVED (was " + source.Secret + ")";
            }
        }


        public class Map_FooItem_FooItemDto{
            public static void Initialize(IMapperConfigurationExpression config)
            {
                config.CreateMap<Foo, FooDto>()
                          .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.TenantFK))
                          .ForMember(t => t.RecordState, opt => opt.MapFrom(s => s.RecordState))
                          .ForMember(t => t.Description, opt => opt.MapFrom(s => s.Text))
                          .ForMember(t => t.ImagePath, opt => opt.MapFrom(s => s.ImagePath))

                          //Either this way (logical strip):
                          //.ForMember(t => t.Secret, opt => opt.MapFrom(s => s.Secret))
                          //.ForMember(t => t.Secret, opt => opt.Condition(x => true))

                          //NO. As per: http://stackoverflow.com/a/30192988/4140558
                          //.ForMember(t => t.TopSecret, opt => opt.ResolveUsing<CustomResolver>())
                          .ForMember(t => t.TopSecret, opt => opt.MapFrom(x => "SECRET REMOVED"))
                          ;


            }
        }
    }

