﻿using AutoMapper;
using FooDemoServer.Shared.Models.DTOs;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Mappers;
using FooDemoServer.Presentation.Mappers.DTOs.V1;

namespace FooDemoServer.Presentation.Mappers
{


    public partial class AutoMapperConfiguration
    {
        public void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                Map_Tenant_TenantDto.Initialize(cfg);
                Map_TenantProperty_TenantPropertyDto.Initialize(cfg);
                Map_TenantClaim_TenantClaimDto.Initialize(cfg);
                Map_User_UserDto.Initialize(cfg);
                Map_UserProperty_UserPropertyDto.Initialize(cfg);
                Map_UserClaim_UserClaimDto.Initialize(cfg);
                Map_Session_SessionDto.Initialize(cfg);
                Map_SessionOperation_SessionOperationDto.Initialize(cfg);

                Map_FooItem_FooItemDto.Initialize(cfg);

                Map_FooSubItem_FooSubItemDto.Initialize(cfg);

                Map_FooItem_SearchResponseItemDto.Initialize(cfg);

                Map_Message_MessageDto.Initialize(cfg);

                Map_Notification_NotificationDto.Initialize(cfg);

                Map_BodyChannel_BodyChannelDto.Initialize(cfg);

                Map_BodyProperty_BodyPropertyDto.Initialize(cfg);

                Map_BodyClaim_BodyClaimDto.Initialize(cfg);

                Map_BodyCategory_BodyCategoryDto.Initialize(cfg);

                Map_BodyAlias_BodyAliasDto.Initialize(cfg);

                Map_Body_BodyDto.Initialize(cfg);

            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}
