﻿using FooDemoServer.Shared;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Application
{

    public class ProjectDbContext : DbContext
    {
        public DbSet<Foo> FooItems { get; set; }
        public DbSet<FooSubItem> FooSubItems { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Body> Bodies { get; set; }

        public DbSet<Spike01Item> Spike01Items { get; set; }
        public DbSet<Spike01SubItem> Spike01SubItems { get; set; }

        public ProjectDbContext() : base("FooProject.Db.CN")
        {
            //Needed to setup for var used in app.config.
            //Needed to setup for var used in app.config.
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(path, "App_Data"));

            base.Configuration.ProxyCreationEnabled = false;
            base.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            new ProjectDbContextModelBuilder().OnModelCreating(modelBuilder);
            base.OnModelCreating(modelBuilder);

        }

    }

}

