﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederBody
    {
        public static void Seed(ProjectDbContext context)
        {

            var records = new Body[] {
                //People:
                new Body {Id=1.ToGuid(), TenantFK=1.ToGuid(), RecordState= RecordPersistenceState.Active,  Type = BodyType.Person, CategoryFK=1.ToGuid(), Key=null, GivenName = "Betty", MiddleNames = "P.", SurName = "Nelson" },
                new Body {Id=2.ToGuid(), TenantFK=1.ToGuid(),  RecordState= RecordPersistenceState.Active,  Type = BodyType.Person, CategoryFK=2.ToGuid(), Key=null, GivenName = "Craig", SurName = "O'Neil"  },
                new Body {Id=3.ToGuid(), TenantFK=1.ToGuid(),  RecordState= RecordPersistenceState.Active,  Type = BodyType.Person, CategoryFK=1.ToGuid(), Key=null, GivenName = "Daniella", SurName = "Pearson", Prefix = "Dr." },
                new Body {Id=4.ToGuid(), TenantFK=1.ToGuid(),  RecordState= RecordPersistenceState.Active,  Type = BodyType.Person, CategoryFK=2.ToGuid(), Key=null, GivenName = "Eric", SurName = "Quail"},
                new Body {Id=5.ToGuid(), TenantFK=1.ToGuid(),  RecordState= RecordPersistenceState.Active,  Type = BodyType.Person, CategoryFK=3.ToGuid(), Key=null, GivenName = "Frank", SurName = "Roberts"},
                //Corps:
                new Body {Id=101.ToGuid(),TenantFK=1.ToGuid(),   RecordState= RecordPersistenceState.Active,  Type = BodyType.Organisation, CategoryFK=3.ToGuid(),  Key="OrgXYZ",  Name="GoodStuff, Inc."},

            };
            context.Set<Body>().AddOrUpdate<Body>(p => p.Id, records);
            context.SaveChanges();

            
            
            
            
            
            
        }
    }




}

