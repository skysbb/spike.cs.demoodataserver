﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederFoo {
        public static void Seed(ProjectDbContext context)
        {
            context.FooItems.AddOrUpdate(p => p.Id,
              new Foo { Id = 1.ToGuid(), TenantFK=1.ToGuid(), Text = "Andy", Secret = "OMG! Secret!", ImagePath = "assets/images/faces/face01.png" },
              new Foo { Id = 2.ToGuid(), TenantFK=1.ToGuid(), Text = "Brian", Secret = "This should never leave!", ImagePath = "assets/images/faces/face02.png" },
              new Foo { Id = 3.ToGuid(), TenantFK = 1.ToGuid(), Text = "Candy", Secret = "The world is falling!", ImagePath = "assets/images/faces/face03.png" }
            );
            context.SaveChanges();

        }
    }


}

