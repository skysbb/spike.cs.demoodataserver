﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederBodyProperty
    {
        public static void Seed(ProjectDbContext context)
        {
            var records = new BodyProperty[] {
                new BodyProperty {Id=1.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=1.ToGuid(), Key="SomePropA",Value="SomeValueA1"},
                new BodyProperty {Id=2.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=1.ToGuid(), Key="SomePropB",Value="SomeValueB1"},
                new BodyProperty {Id=3.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=1.ToGuid(), Key="SomePropC",Value="SomeValueC1"},
                new BodyProperty {Id=21.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=2.ToGuid(), Key="SomePropA",Value="SomeValueA2"},
                new BodyProperty {Id=22.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=2.ToGuid(), Key="SomePropB",Value="SomeValueB3"},
                new BodyProperty {Id=23.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=2.ToGuid(), Key="SomePropC",Value="SomeValueC4"},
                new BodyProperty {Id=31.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=3.ToGuid(), Key="SomePropA",Value="SomeValueA"},
                new BodyProperty {Id=41.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=4.ToGuid(), Key="SomePropA",Value="SomeValueA"},
                new BodyProperty {Id=51.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=5.ToGuid(), Key="SomePropA",Value="SomeValueA"},

                new BodyProperty {Id=101.ToGuid(), TenantFK=1.ToGuid(), OwnerFK=101.ToGuid(), Key="SomePropA",Value="SomeValueA"},
                };
            context.Set<BodyProperty>().AddOrUpdate<BodyProperty>(p => p.Id, records);
            context.SaveChanges();
        }
    }


}

