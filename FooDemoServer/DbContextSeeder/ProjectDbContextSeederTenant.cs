﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederTenant

    {
        public static void Seed(ProjectDbContext context)
        {

            var records = new Tenant[] {
                //People:
                new Tenant {Id=1.ToGuid(), Enabled=true, Key="OrgA", DisplayName="Org A, Inc."},
                new Tenant {Id=2.ToGuid(),  Enabled=true, Key="OrgB", DisplayName="Org B, Inc."},

            };
            context.Set<Tenant>().AddOrUpdate<Tenant>(p => p.Id, records);
            context.SaveChanges();
        }
    }


}
