﻿using FooDemoServer.Application;
using FooDemoServer.Services;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Data.Entity.Migrations;
using System.Linq;

namespace FooDemoServer.DbContextSeeder
{


    public class ProjectDbContextSeederSchoolSets
        {

            public static void Seed(ProjectDbContext context)
            {



                SchoolCsvImporterService source = new SchoolCsvImporterService();
                int counter = 0;
                int schoolBaseCounter = 1000;
                int principalBaseCounter = 2000;
                foreach (SchoolDescriptionRaw schoolDescriptionRaw in source.Import()) //  SchoolSeedingData.data.Take(10))
                {
                    counter++;
                    if (counter > 100) { continue; }

                    int schoolId = (int)decimal.Parse(schoolDescriptionRaw.SchoolID);
                    Guid schoolGuid = (schoolBaseCounter + schoolId).ToGuid();
                    Guid principalGuid = (principalBaseCounter + schoolId).ToGuid();


                    //Create the Org backing the School:
                    var schoolBody = new Body
                    {
                        Id = schoolGuid,
                        TenantFK = 1.ToGuid(),
                        RecordState = RecordPersistenceState.Active,
                        CategoryFK = 1.ToGuid(),
                        Type = BodyType.Organisation,
                        Name = schoolDescriptionRaw.Name
                    };

                    //Add a Name to the Org, then make it Preferred:

                    //Save after saving BodyName:
                    context.Set<Body>().AddOrUpdate(p => p.Id, schoolBody);

                    var schoolName = new BodyAlias {
                        Id = schoolGuid,
                        TenantFK = 1.ToGuid(),
                        OwnerFK = schoolBody.Id, 
                        Title = schoolDescriptionRaw.Name,
                        Name = schoolDescriptionRaw.Name };
                    context.Set<BodyAlias>().AddOrUpdate(p => p.Id, schoolName);

                    //schoolBody.PreferredName = schoolName;



                    ////Add a chanel to the Org:
                    var orgChannel = new BodyChannel
                    {
                        Id = schoolGuid,
                        TenantFK = 1.ToGuid(),
                        OwnerFK = schoolGuid,
                        Title = "Office Phone",
                        Protocol = BodyChannelType.Phone,
                        Address = schoolDescriptionRaw.Telephone
                    };
                    context.Set<BodyChannel>().AddOrUpdate(p => p.Id, orgChannel);

                    ////Ensure the Channel is part of the Org:
                    //if (!schoolBody.Channels.Any(x => x.Id == orgChannel.Id))
                    //{
                    //    schoolBody.Channels.Add(orgChannel);
                    //}




                    //Add claims
                    if (schoolBody.Claims.SingleOrDefault(x => x.Key == "Longitude") == null)
                    {
                        schoolBody.Claims.Add(new BodyClaim { TenantFK=1.ToGuid(), Authority = "Foo", AuthoritySignature = "Bar", Key = "Longitude", Value = schoolDescriptionRaw.Longitude });
                    }
                    if (schoolBody.Claims.SingleOrDefault(x => x.Key == "Latitude") == null)
                    {
                        schoolBody.Claims.Add(new BodyClaim { TenantFK = 1.ToGuid(), Authority = "Foo", AuthoritySignature = "Bar", Key = "Latitude", Value = schoolDescriptionRaw.Latitude });
                    }

                    if (schoolBody.Properties.SingleOrDefault(x => x.Key == "Longitude") == null)
                    {
                        schoolBody.Properties.Add(new BodyProperty { TenantFK = 1.ToGuid(), Key = "Longitude", Value = schoolDescriptionRaw.Longitude });
                    }
                    if (schoolBody.Properties.SingleOrDefault(x => x.Key == "Latitude") == null)
                    {
                        schoolBody.Properties.Add(new BodyProperty { TenantFK = 1.ToGuid(), Key = "Latitude", Value = schoolDescriptionRaw.Latitude });
                    }



                    ////Now do the principal:
                    var principal = new Body
                    {
                        Id = principalGuid,
                        TenantFK = 1.ToGuid(),
                        Type = BodyType.Person,
                        CategoryFK = 2.ToGuid(),
                        Name = schoolDescriptionRaw.Principal
                    };

                    context.Set<Body>().AddOrUpdate(p => p.Id, principal);



                    //Make a school with both Org and Principal:
                    var school = new School { Id = schoolGuid, TenantFK = 1.ToGuid(), Type = SchoolEstablishmentType.School, Key = schoolDescriptionRaw.Name, OrganisationFK = schoolBody.Id, PrincipalFK = principal.Id };
                    context.Set<School>().AddOrUpdate(p => p.Id, school);





                }


                context.SaveChanges();

            }



        }


}

