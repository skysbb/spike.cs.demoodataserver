﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederBodyName
    {
        public static void Seed(ProjectDbContext context)
        {
            var records = new BodyAlias[] {
                //People:
                new BodyAlias {Id=6.ToGuid(),  TenantFK=1.ToGuid(), RecordState= RecordPersistenceState.Active,  DisplayOrderHint=0, Name=null, GivenName = "Franky", SurName = "Roberts", OwnerFK=5.ToGuid() },
                //Corps:
                new BodyAlias {Id=102.ToGuid(),  TenantFK=1.ToGuid(), RecordState= RecordPersistenceState.Active, DisplayOrderHint=0, Name="GS, Inc.", OwnerFK=101.ToGuid() },
            };
            context.Set<BodyAlias>().AddOrUpdate<BodyAlias>(p => p.Id, records);

        }
    }


}

