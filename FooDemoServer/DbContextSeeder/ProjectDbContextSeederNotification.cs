﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederNotification
    {
        public static void Seed(ProjectDbContext context)
        {


            context.Set<Notification>().AddOrUpdate<Notification>(p => p.Id,
              new Notification { Id = 1.ToGuid(), TenantFK=1.ToGuid(), Type = NotificationType.Notification, Level = TraceLevel.Info, Read = false, Text = "Some Message about Foo.", DateTimeCreatedUtc = DateTime.UtcNow },
              new Notification { Id = 2.ToGuid(), TenantFK=1.ToGuid(), Type = NotificationType.Message, Level = TraceLevel.Warn, Read = false, Text = "Another Message for you.", DateTimeCreatedUtc = DateTime.UtcNow },
              new Notification { Id = 3.ToGuid(), TenantFK = 1.ToGuid(), Type = NotificationType.Task, Level = TraceLevel.Info, Read = true, Text = "A Message you've read.", DateTimeCreatedUtc = DateTime.UtcNow, DateTimeReadUtc = DateTime.UtcNow }
            );
            context.SaveChanges();


        }
    }


}

