﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederBodyCategory
    {
        public static void Seed(ProjectDbContext context)
        {
            var records = new BodyCategory[] {
                new BodyCategory {Id=1.ToGuid(), TenantFK=1.ToGuid(),  RecordState = RecordPersistenceState.Active, Text="Cat1"},
                new BodyCategory {Id=2.ToGuid(), TenantFK=1.ToGuid(),  RecordState = RecordPersistenceState.Active, Text="Cat2"},
                new BodyCategory {Id=3.ToGuid(), TenantFK=1.ToGuid(),  RecordState = RecordPersistenceState.Active, Text="Cat3"}
            };
            context.Set<BodyCategory>().AddOrUpdate<BodyCategory>(p => p.Id, records);
            context.SaveChanges();
        }
    }


}

