﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederMessage
    {
        public static void Seed(ProjectDbContext context)
        {
            context.Set<Message>().AddOrUpdate<Message>(p => p.Id,
              new Message { Id = 1.ToGuid(),TenantFK=1.ToGuid(),  Level = TraceLevel.Info, Read = false, Text = "Some Message about Foo.", DateTimeCreatedUtc = DateTime.UtcNow },
              new Message { Id = 2.ToGuid(),TenantFK=1.ToGuid(),  Level = TraceLevel.Warn, Read = false, Text = "Another Message for you.", DateTimeCreatedUtc = DateTime.UtcNow },
              new Message { Id = 3.ToGuid(), TenantFK = 1.ToGuid(), Level = TraceLevel.Info, Read = true, Text = "A Message you've read.", DateTimeCreatedUtc = DateTime.UtcNow, DateTimeReadUtc = DateTime.UtcNow }
            );
            context.SaveChanges();
        }
    }


}

