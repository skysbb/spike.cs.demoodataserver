﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederSession

    {
        public static void Seed(ProjectDbContext context)
        {
            var records = new Session[] {
                //People:
                new Session {Id=1.ToGuid(), Enabled=false, StartDateTimeUtc=DateTime.UtcNow.AddDays(-3), UserFK=1.ToGuid(), TenantFK=1.ToGuid()},
                new Session {Id=2.ToGuid(), Enabled=false, StartDateTimeUtc=DateTime.UtcNow.AddDays(-6), UserFK=1.ToGuid(), TenantFK=1.ToGuid()},
            };
            context.Set<Session>().AddOrUpdate<Session>(p => p.Id, records);
            context.SaveChanges();
        }
    }
}
