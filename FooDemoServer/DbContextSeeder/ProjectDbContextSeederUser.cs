﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.DbContextSeeder
{

    public static class ProjectDbContextSeederUser

    {
        public static void Seed(ProjectDbContext context)
        {

            var records = new User[] {
                //People:
                new User {Id=0.ToGuid(), Enabled=true, DisplayName="Anon"},
                new User {Id=1.ToGuid(), Enabled=true, DisplayName="J Smith"},
                new User {Id=2.ToGuid(), Enabled=true, DisplayName="P Smith"},
            };
            context.Set<User>().AddOrUpdate<User>(p => p.Id, records);
            context.SaveChanges();
        }
    }
}
