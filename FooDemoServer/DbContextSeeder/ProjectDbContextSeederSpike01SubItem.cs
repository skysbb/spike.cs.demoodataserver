﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public class ProjectDbContextSeederSpike01SubItem
    {

        public static void Seed(ProjectDbContext context)
        {

            var records = new List<Spike01SubItem>();

            for (int i = 1; i <= 200; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    int recordId = (100 * i) + j;

                    records.Add(new Spike01SubItem
                    {
                        Id = recordId.ToGuid(),
                        TenantFK = 1.ToGuid(),
                        ParentFK = i.ToGuid(),
                        DisplayOrderHint = j,
                        Protocol = Protocol.Phone,
                        Title = "Tit...[" + i + "]",
                        Address = "Addre...[" + i + "]",
                        Street = "Stre...[" + i + "]",
                        City = "City...[" + i + "]",
                        Country = "Country [" + i + "]"
                    });
                }
            }

            context.Set<Spike01SubItem>().AddOrUpdate<Spike01SubItem>(p => p.Id, records.ToArray());
            context.SaveChanges();

        }

    }
}

