﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace FooDemoServer.DbContextSeeder
{
    public class ProjectDbContextSeederSpike01Item
    {

        public static void Seed(ProjectDbContext context)
        {
            var records = new List<Spike01Item>();

            for (int i=1;i<=200;i++)
            {
                var even=  (i % 2)==0;

                records.Add(new Spike01Item
                {
                    Id = i.ToGuid(),
                    TenantFK = 1.ToGuid(),
                    IsOrg = even,
                    OrgName = even ? "Some OrgName [" + i + "]" : null,

                    First = (even) ? null : "First [" + i + "]",
                    Middles = (even) ? null : "M[" + i + "]",
                    Last = (even) ? null : "First [" + i + "]",

                    Description = "...blah..." + i,
                    Notes = "Some longer blah...blah...blah...blah..."
                });
            }
            context.Set<Spike01Item>().AddOrUpdate<Spike01Item>(p => p.Id, records.ToArray());
            context.SaveChanges();
        }
    }


}

