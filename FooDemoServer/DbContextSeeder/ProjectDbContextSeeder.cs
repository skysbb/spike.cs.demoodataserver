﻿using FooDemoServer.Application;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.DbContextSeeder
{


    public class ProjectDbContextSeeder
    {

        public static void Seed(ProjectDbContext context)
        {
            //Make sure Tenant is before User, whicch is before Sesssion, which is before all other Records.
            ProjectDbContextSeederTenant.Seed(context);
            ProjectDbContextSeederUser.Seed(context);
            ProjectDbContextSeederSession.Seed(context);

            ProjectDbContextSeederFoo.Seed(context);
            ProjectDbContextSeederFooSubItems.Seed(context);

            ProjectDbContextSeederMessage.Seed(context);
            ProjectDbContextSeederNotification.Seed(context);

            ProjectDbContextSeederBodyCategory.Seed(context);
            ProjectDbContextSeederBodyName.Seed(context);
            ProjectDbContextSeederBody.Seed(context);
            ProjectDbContextSeederBodyProperty.Seed(context);
            ProjectDbContextSeederBodyClaim.Seed(context);
            ProjectDbContextSeederSchoolSets.Seed(context);

            ProjectDbContextSeederSpike01Item.Seed(context);
            ProjectDbContextSeederSpike01SubItem.Seed(context);
        }
    }


}

