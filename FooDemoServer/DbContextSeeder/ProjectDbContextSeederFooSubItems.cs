﻿using FooDemoServer.Application;
using FooDemoServer.Shared.Models.Entities;
using System.Data.Entity.Migrations;
using System.Linq;

namespace FooDemoServer.DbContextSeeder
{
    public static class ProjectDbContextSeederFooSubItems
    {
        public static void Seed(ProjectDbContext context)
        {
            var a = context.FooItems.First();

            context.FooSubItems.AddOrUpdate(p => p.Id,
              new FooSubItem { Id = 1.ToGuid(), TenantFK=1.ToGuid(), ParentFK = a.Id, Text = "SubAndy" },
              new FooSubItem { Id = 2.ToGuid(), TenantFK=1.ToGuid(), ParentFK = a.Id, Text = "SubBrian" },
              new FooSubItem { Id = 3.ToGuid(), TenantFK = 1.ToGuid(), ParentFK = a.Id, Text = "SubCandy" }
            );
            context.SaveChanges();
        }
    }


}

