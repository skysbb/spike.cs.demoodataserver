﻿using FooDemoServer.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData;
using System.Data.Entity;

namespace FooDemoServer.Presentation.Controllers
{
    public abstract class ODataControllerBase<T> : ODataControllerBase where T :class {

        protected DbSet<T> _dbSet
        {
            get
            {
                return _dbConnection.Set<T>();
            }
        }

        public ODataControllerBase()
        {
        }
    }

        public abstract class ODataControllerBase : ODataController
    {
        protected ProjectDbContext _dbConnection;


        public ODataControllerBase()
        {
            _dbConnection = new ProjectDbContext();
            _dbConnection.Database.Log = Console.Write;
        }




        protected override void Dispose(bool disposing)
        {
            _dbConnection.SaveChanges();

            if (_dbConnection != null)
            {
                _dbConnection.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
