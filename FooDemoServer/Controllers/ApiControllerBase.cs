﻿using FooDemoServer.Application;
using System;
using System.Web.Http;

namespace FooDemoServer.Presentation.Controllers
{
    public abstract class ApiControllerBase : ApiController {
        protected ProjectDbContext _dbConnection;
        public ApiControllerBase()
        {
            _dbConnection = new ProjectDbContext();
            _dbConnection.Database.Log = Console.Write;
        }

        protected override void Dispose(bool disposing)
        {
            _dbConnection.SaveChanges();

            if (_dbConnection != null)
            {
                _dbConnection.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
