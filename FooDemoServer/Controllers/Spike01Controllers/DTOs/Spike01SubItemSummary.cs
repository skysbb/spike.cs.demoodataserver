﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Controllers.Spike01Controllers.DTOs
{
    //Class to render subitems in the Spike01Item Edit/View components.
    public class Spike01SubItemSummary : IHasGuidId
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
