﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Controllers.Spike01Controllers.DTOs
{
    //Class to render items in the Search/Browse view
    public class Spike01ItemSummary : IHasGuidId
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
