﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FooDemoServer.Shared.Models.Contracts;

namespace FooDemoServer.Controllers.Spike01Controllers.DTOs
{
    //DTO for passinging new order have having dragged around things on the screen.

    public class Spike01SubItemOrderUpdate : IHasGuidId
    {
        // Id of parent object (ie Spike01Item)
        public Guid Id { get; set; }
        // If of child object within list (Spike01SubItem that has just been moved)
        public Guid SubItemId { get; set; }
        // New Position from top (0 based)
        public int NewPosition { get; set; }

    }
}
