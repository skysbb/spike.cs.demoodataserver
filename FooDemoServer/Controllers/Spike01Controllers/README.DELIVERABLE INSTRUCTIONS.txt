﻿
Capabilities
============
The developer/team must be able to develop frontend using Angular 4+, and the backend using ASP.NET, ODataControllers, CodeFirst.


Purpose
=======
The purpose of the spike is to demonstrate using Angular over ASP.MVC Odata controllers a clean BREAD (Browse/Retrieve/Edit/Add/Delete) workflow, taking into account Parent/Child records.

Examples of such a workflow might be:
* Person, with subitems of Addresses <-- let's start here.
* Project, with subitems of Tasks
* Task, with subitems of Tasks
* etc.

For a diagram of the workflow desired, see:
http://skysigal.com/diagrams/BREAD/home


Repositories
============
For the backend, clone:
https://bitbucket.org/skysigal/spike.cs.demoodataserver
and continue the work already started within Spike01ItemController
https://bitbucket.org/skysigal/spike.cs.demoodataserver/src/961a1c778ab975e59b8c3df3f7eba6373433c62a/FooDemoServer/Controllers/Spike01Controllers/?at=master

For the frontend, clone
https://bitbucket.org/skysigal/spike.ng.06
And take over the following page and start from there.
Spike.Ng.06 / src / app / dev / modules / spike / dev.spike.03.dragula.view.component.ts



NFRs
====
* MUST: Datastorage: Use CodeFirst. Using an inmem store is fine too. Just no stored procs or db *.sql scripts to get going.
* MUST: Use ASP.MVC OdataControllers for backend. Because it will save you time (you won't have to make different endpoints)
* MUST: Use Angular 4+ for frontend (not Angular.JS)
* MUST: Develop in Typescript -- not JS.
* MUST: Use Angular/CLI to setup project (to save yourself time designing gulp/etc.)
* COULD: Front end and back end unit tests are great - but not required for this demo project.
* COULD: ASP.NET Core can be used for backend, but not required. Probably will make it harder for me to understand, as I have not been able yet to invest time into it.
* SHOULD: Work with existing provided front and back repositories -- but if you prefer, work from scratch (I'll be copying them over later)
* MUST NOT use integers for Db Record Ids. Use Guids. You'll see how I use the constructor to set the Id in entity models.
* MUST follow the UIs layout..ie just make it look like the rest of the client.



Key Deliverables
================
Your branch of the above Code repositories to include what is required.


Your work would include the following:
* Server Models already exist for the following, so now create Client (TS) Models:
    * Spike01ItemSummary <- used to display records in the Browse view. Not that this is a combination of data from Spike01Item and the top Spike01SubItem
    * Spike01Item <- used for View and Edit of a single Spike01Item
    * Spike01SubItemSummary <- used for displaying list of drag/dropable child records within Spike01ItemRetrieveView and Spike01ItemEditView
    * Spike01SubItem <- used for View/Edit of a SubItem. 

* Views/Components to be created to render the above models are:
    * Spike01ItemSummaryBrowseView
    * Spike01ItemRetrieveView
    * Spike01ItemEditView (with nested Spike01SubItemSummaryBrowseView list)  (reuse for Add)
    * Spike01SubItemRetrieveView
    * Spike01SubItemEditView
	  * Reuse for Spike01SubItemAddView
	* Confirmation Modal before deleting records.

Details for Views
=================
* Spike01ItemSummaryBrowseView: this is a View Component that uses the API to search for Items that have a specific value.
  * Specifications are:
    * MUST have a Search field at top of form, and button to submit Search.
    * MUST fetch and show only 10 records that match.
	* MUST show checkbox at left of each record that is shown.
    * EVENTS HANDLING:
	  * MUST retrieve records via Odata API on submitting search text.
	  * MUST: trigger request for next 10 records when one has scrolled down to last available record ("Infinity Scrolling").
	    * MUST NOT use Page Next. Absolutely hate Paging using Last/Next.
      * MUST handle click of record (eg: on an View icon) to VIEW a single record in Spike01ItemRetrieveView.
	    * SHOULD track what record was clicked, so that upon return from Spike01ItemRetrieveView, can see where we started.
	  * MUST provide a button top or bottom to "DELETE SELECTED", which would delete all checked items.
	  * MUST provide "ADD NEW ITEM" button, to show blank Spike01ItemEditView ready to fill in.
	    * When finished adding new record, must return to this Browse View.
		* SHOULD clear list, and show the -- highlighted -- newly added record.
* Spike01ItemRetrieveView: this is a View Component to VIEW a single Spike01Item in Non-Edit mode.
  * Specifications are:
    * MUST show Spike01Item record values vertically (it's a small db record that might fit all on one line, but the data should be laid out as for most records, with fields on top of each other.)
	* MUST display under the Spike01Item fields, a list of Spike01SubItems records.
	* MUST ensure the list of Spike01SubItems is drag/droppable in order to rearrange the list's display order.
	  * Drop event must fire off invocation of provided API endpoint, to save new order (see below, EVENT HANDLING).
	* MUST: each non-editable Spike01SubItem must be rendered as non-editable text template (ie, no form fields, just divs/spans at this point). 
	  * SHOULD: the idea is to use an external template to render the subitem that for/each through the list and render.
	* EVENT HANDLING:
      * MUST provide a clickable icon to RETURN to Spike01ItemSummaryBrowseView.
	  * MUST provide a clickable icon to EDIT the record, by going to Spike01ItemEditView.
	  * MUST provide a clickable icon to ADD NEW Spike01SubItem (eg: add another Invoice/etc.) and show Spike01SubItemEditView.
	    * MUST: When finished, return to this View, 
	       * SHOULD: and with SubItem highlighted.
   	    * MUST: each non-editable Spike01SubItem record can be clicked in order to edit the single Spike01SubItem.
	    * MUST: each non-editable Spike01SubItem must provide a DELETE button to delete the single Spike01SubItem.
* Spike01ItemEditView: this is a View Component to EDIT a single Spike01Item in Edit mode. 
    * MUST show form fields vertically (again, this is a small demo field, but lay the form fields vertically, in the traditional layout.)
	* MUST provide a SAVE button, which returns to Spike01ItemRetrieveView.
	* MUST provide a CANCEL button , which returns to Spike01ItemRetrieveView.
	* MUST show same list of Spike01SubItems as was shown in Spike01ItemRetrieveView.
	* MUST allow for reordering of Spike01SubItem elements, just as was done in Spike01ItemRetrieveView.
	  * MUST: submit reorder request to provided API backend.
* Spike01SubItemRetrieveView: this is the View used to view a single Spike01SubItem in non-edit mode.
  * SHOULD: use a template to lay it out.
  * Event Handling:
	* MUST: provide a button to CANCEL/RETURN, and return to where it came from. Which, depending on source, can be:
	  * Spike01ItemRetrieveView
	  * Spike01ItemEditView
    * MUST: Provide a button to EDIT the Record. Which takes you Spike01SubItemEditView
* Spike01SubItemEditView: this is a View to EDIT a single Spike01SubItem.
  * Event Handling:
    * MUST: provide a CANCEL/RETURN button, and return to Spike01SubItemRetrieveView.
	* MUST: provide a SAVE button, and return to Spike01SubItemRetrieveView.
	  * COULD: Maybe skip returning to  Spike01SubItemRetrieveView and return to Spike01ItemRetrieveView or Spike01ItemEditView, depending on where origin came from.
	    * Highlight newly edited/added subitem.

STATE
=====
To ease flow around the above views, you'll need to keep some client state. 
I'd make a shared Service to back the views, and set/read the following properties
as required:
    * SelectedSpike01Items : Spike01Items[] (Highlight Type 1)
      * Used by 'DELETE SELECTED'
    * LastClicked Spike01Item 
      * Used to highlight the record in the Browse View, after returning from Spike01ItemRetrieveView
    * LastClickedSpike01SubItem
      * Used to highlight the record in the Spike01ItemRetrieveView/Spike01ItemEditView's list of drag/droppable Spike01SubItemRetrieveView items,
    * Spike01ItemSourceView
      * marker as to where to return to after viewing a single Spike01SubItemRetrieveView as one can get to Spike01SubItemRetrieveView from one of two places: 
        * Spike01ItemRetrieveView
	    * Spike01ItemEditView





MILESTONES 1: 
: Setup and running both Front end and backend, and make a tiny change to the pre-existing Client Component, 
  to demonstrate you found the View/Area  where the work is to be done.
  * Deliverables:
    * screengrab of Client up and running
	* screengrap of Client folder open in VS Code, so we are both in agreement where you'll be adding the Client Views.
	* screengrab of browser correctly showing JSON from an endpoint, so we are both in agreement as to Controller to be used.
	* A code commit to each repo, as a branch, proving we have a way to submit work.

MILESTONE 2: 
  * Screen grab of Search Form, correctly retrieving and displaying Spike01ItemSummary items, with Infinite Scroll working to retrieve the next page.
  * Code committed.

MILESTONE 3: 
  * Screen grab of Spike01ItemRetrieveView, showing text values from SpikeO1Item AND the nested Spike01SubItemSummaryBrowseView component, which shows Drag/Dropping of  Spike01SubItemSummary event being saved.
  * Screen grab of returning to highlighted record in Spike01ItemSummaryBrowseView
  * Code committed.

MILESTONE 4: 
  * Screen grab of Spike01ItemEditView, showing edit fields, saving SpikeO1Item  record, and reusing the drag/drop  Spike01SubItemSummaryBrowseView list component. 
  * Screen grab of reusing the Edit form, but for adding a record.
  * Code committed.

MILESTONE 4: 
  * Screen grab of reusing  Spike01ItemEditView to Add a new Spike01Item record (at this stage, ok to do so with no child Spike01SubItem).
  * Screen grab of implementation of checkbox + MultipleDelete feature from the Spike01ItemSummaryBrowseView
  * Screen grab of reusing the Edit form, but for adding a record.
  * Code committed.

MILESTONE 5: 
  * Screen grab of Spike01SubItemEditView, showing edit fields, saving SpikeO1SubItem record, and reusing the drag/drop  Spike01SubItemSummaryBrowseView list component 
  * Screen grab of reusing Spike01SubItemEditView for adding new record.
  * copy and reimplement Deleting of several Spike01SubItem records.
  * Code committed.

  