﻿using FooDemoServer.Controllers.Spike01Controllers.DTOs;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData;

namespace FooDemoServer.Presentation.Controllers
{
    public class Spike01ItemSummaryController : ApiControllerBase
    {
        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        //[EnableQuery(PageSize = 100)]
        public Spike01ItemSummary[] Get(string searchTerm)
        {
            var tmpRecords = _dbConnection.Set<Spike01Item>().Where(x =>
                ((x.OrgName != null) && (x.OrgName.Contains(searchTerm)))
                ||
                ((x.First != null) && (x.First.Contains(searchTerm)))
                ||
                ((x.Last != null) && (x.Last.Contains(searchTerm)))
                )
                .Include(x => x.Items).ToArray();

            List<Spike01ItemSummary> results = new List<Spike01ItemSummary>();

            foreach (var tmp in tmpRecords)
            {
                string title = (tmp.IsOrg) ? tmp.OrgName : tmp.First + " " + tmp.Last;
                string description = null;

                if (tmp.Items.Count > 0)
                {
                    var tSubItem = tmp.Items.First();

                    description = !string.IsNullOrEmpty(tSubItem.Address) ? tSubItem.Address : tSubItem.Street + " " + tSubItem.City + " " + tSubItem.Country;
                }
                results.Add(new Spike01ItemSummary { Id = tmp.Id, Title = title, Description = description });
            }
            return results.ToArray();
        }
    }

    public class Spike01SubItemSummaryController : ApiControllerBase
    {
        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        [EnableQuery(PageSize = 100)]
        public Spike01SubItemSummary[] Get(Guid key)
        {

            var tmpRecords = _dbConnection.Set<Spike01SubItem>()
                .Where(x => x.ParentFK == key)
                .Select(x => new Spike01SubItemSummary { Id = x.Id, Title = x.Protocol.ToString(), Description = !string.IsNullOrEmpty(x.Address) ? x.Address : x.Street + " " + x.City + " " + x.Country })
                .ToArray();

            return tmpRecords;
        }
    }
}

