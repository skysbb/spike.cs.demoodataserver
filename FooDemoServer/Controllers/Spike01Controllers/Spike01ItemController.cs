﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData;
using System.Data.Entity;
using FooDemoServer.Shared.Models.Entities;
using System.Web.Http;

namespace FooDemoServer.Presentation.Controllers
{
    public class Spike01ItemController : ODataControllerBase
    {

        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        [EnableQuery(PageSize = 100)]
        public IQueryable<Spike01Item> Get()
        {
            IQueryable<Spike01Item> results;

            results =
                _dbConnection.Spike01Items
                .Include(x => x.Items);
            ;

            return results;

        }

        public Spike01Item Get(Guid key)
        {
            Spike01Item result;

            result =
                _dbConnection.Spike01Items
                .SingleOrDefault(x=>x.Id == key)
                //.Include(x => x.Items)
                ;
            ;

            return result;

        }

            public void Post([FromBody]Spike01Item item)
        {
            //Validate.
            //Check that item.Id != Guid.Empty,
            //etc.

            //Add...
            // Note that works most cases -- as long as there is not a 
            // db constraint (eg: If Item MUST have a LineItem Z(db constraint)...then I'm not sure how this could be posted)
            _dbConnection.Spike01Items.Add(item);
        }


        public void Put([FromBody]Spike01Item item)
        {
            var single = _dbConnection.Spike01Items.SingleOrDefault(x => x.Id == item.Id);

            if (single == null)
            {
                return;
            }
            // Now what?

        }


    }
}

