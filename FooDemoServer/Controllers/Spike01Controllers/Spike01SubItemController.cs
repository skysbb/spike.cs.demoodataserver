﻿using FooDemoServer.Shared.Models.Entities;
using System;
using System.Linq;
using System.Web.OData;
using System.Data.Entity;
using System.Web.Http;

namespace FooDemoServer.Presentation.Controllers
{
    public class Spike01SubItemController : ODataControllerBase
    {

        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        [EnableQuery(PageSize = 100)]
        public IQueryable<Spike01SubItem> Get()
        {
            IQueryable<Spike01SubItem> results;

            results =
                _dbConnection.Spike01SubItems;
            ;

            return results;

        }

        public Spike01SubItem Get(Guid key)
        {
            Spike01SubItem result;

            result =
                _dbConnection.Spike01SubItems
                .SingleOrDefault(x => x.Id == key)
                //.Include(x => x.Items)
                ;
            ;

            return result;

        }

        public void Post([FromBody]Spike01SubItem item)
        {
            //Validate.
            //Check that item.Id != Guid.Empty,
            //etc.

            //Add...
            // Note that works most cases -- as long as there is not a 
            // db constraint (eg: If Item MUST have a LineItem Z(db constraint)...then I'm not sure how this could be posted)
            _dbConnection.Spike01SubItems.Add(item);
        }


    }

}

 



