﻿using FooDemoServer.Controllers.Spike01Controllers.DTOs;
using FooDemoServer.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FooDemoServer.Controllers.Spike01Controllers
{
    // Questions:
    // To change Order of Spike01SubItems from UI, should we really be posting back whole collection of SubItems.
    // Probably not: big post for such a little update.
    // So instead, maybe (???) it should be a separate 
    // Spike01SubItemOrderUpdateController be developed in order to post back a
    // SubItemOrderUpdate object (eg: {Id=[the guid of the SubItem], Position=[New Position on Screen]}
    // for this controller to fetch collection.
    // But that means client should have updated their interface to keep same order... Doable. Best?
    // 
    public class Spike01SubItemOrderUpdateController : ApiControllerBase // ODataControllerBase
    {
        //POST command of 


        public void Post([FromBody]Spike01SubItemOrderUpdate subItemOrderUpdate)
        {
            //Fetch SubItems connected to Paren Id
            var subItems = _dbConnection.Spike01SubItems
                .Where(x => x.ParentFK == subItemOrderUpdate.Id)
                .OrderBy(x => x.DisplayOrderHint)
                .ToList();

            var singleItem = subItems.SingleOrDefault(x => x.Id == subItemOrderUpdate.SubItemId);

            if (singleItem == null)
            {
                return;
            }
            subItems.Remove(singleItem);


            subItems.Insert(subItemOrderUpdate.NewPosition, singleItem);
            var counter = 0;
            foreach (var item in subItems)
            {
                item.DisplayOrderHint = counter++;
            }

            //Changes will be saved by base.
        }
    }
}
