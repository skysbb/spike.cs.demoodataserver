﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;

namespace FooDemoServer.Controllers
{
    //[ODataRoutePrefix("search")]
    public class GeoDataDtoController : ODataController
    {


        private ProjectDbContext _dbConnection;



        public GeoDataDtoController()
        {
            _dbConnection = new ProjectDbContext();
            _dbConnection.Database.Log = Console.Write;
        }


        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery(PageSize=100)]
        public IQueryable<SearchResponseItemDto> Get(string type, string searchTerm)
        {

            IQueryable< SearchResponseItemDto > results;
            if (type != null) { type = type.ToLower(); }
            switch (type)
            {
                case "foo":
                case "fooItem":
                    if (string.IsNullOrWhiteSpace(searchTerm))
                    {
                        results = _dbConnection.FooItems.Take(100).ProjectTo<SearchResponseItemDto>();
                    }
                    else
                    {
                        results = _dbConnection.FooItems.Where(x => x.Text.Contains(searchTerm)).Take(100).ProjectTo<SearchResponseItemDto>();
                    }
                    break;
                default:
                    results = new SearchResponseItemDto [0].AsQueryable();
                    break;
            }

            return results;

        }

        //public void Get(System.Web.OData.ODataQueryOptions options)
        //{

        //}

        // GET api/values/5 
        //[ApplyProxyDataContractResolverAttribute]
        public FooItem Get(Guid id)
        {
            using (var db = new ProjectDbContext())
            {
                return db.FooItems.SingleOrDefault(x=>x.Id==id);
            }
        }

        // POST api/values 
        public void Post([FromBody]FooItem value)
        {
        }

        // PUT api/values/5 
        public void Put(int id, [FromBody]FooItem value)
        {
        }

        // DELETE api/values/5 
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (_dbConnection!=null)
            {
                _dbConnection.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}