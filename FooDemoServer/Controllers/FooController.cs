﻿//using FooDemoServer.Shared;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Data.Entity;
//using System.Web.OData;
//using FooDemoServer.Shared.Models.Entities;

//namespace FooDemoServer.Presentation.Controllers
//{
//    public class FooController : ODataController
//    {
//        // GET api/values 
//        //[ApplyDataContractResolver]
//        //[ApplyProxyDataContractResolverAttribute]
//        [EnableQuery]
//        public IEnumerable<Foo> Get()
//        {
//            using (var db = new ProjectDbContext())
//            {
//                var result = db.FooItems.Include(x=>x.Items).ToList<Foo>();
//                return result;
//            }
//        }

//        // GET api/values/5 
//        //[ApplyProxyDataContractResolverAttribute]
//        [EnableQuery]
//        public Foo Get(Guid key)
//        {
//            using (var db = new ProjectDbContext())
//            {
//                return db.FooItems.SingleOrDefault(x=>x.Id==key);
//            }
//        }

//        // POST api/values 
//        public void Post([FromBody]Foo value)
//        {
//        }

//        // PUT api/values/5 
//        public void Put(int id, [FromBody]Foo value)
//        {
//        }

//        // DELETE api/values/5 
//        public void Delete(int id)
//        {
//        }
//    }
//}