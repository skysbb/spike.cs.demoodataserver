﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Application;
using FooDemoServer.Controllers;

namespace FooDemoServer.Presentation.Controllers
{
    [ODataRoutePrefix("search")]
    public class SearchResponseItemDtoController : ODataControllerBase
    {

        



        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery(PageSize=100)]
        public IQueryable<SearchResponseItemDto> Get(string types, string searchTerm)
        {
            if ((string.IsNullOrWhiteSpace(types))||(types == "undefined"))
            {
                types = "*";
            }
            if (types == "*")
            {
                types = "fooitem,body";
            }
            types = types.ToLower().Trim();

            var results = new List<SearchResponseItemDto>();

            foreach (var tmp in types.Split(new char[] { ',', ';', '|' }))
            {
                var tmp2 = tmp.Trim();
                if (string.IsNullOrWhiteSpace(tmp2))
                {
                    continue;
                }

                switch (tmp2)
                {
                    //case "foo":
                    //case "fooitem":
                        //if (string.IsNullOrWhiteSpace(searchTerm))
                        //{
                        //    results.AddRange(_dbConnection.FooItems.Take(100).ProjectTo<SearchResponseItemDto>().ToArray());
                        //}
                        //else
                        //{
                        //    results.AddRange(_dbConnection.FooItems.Where(x => x.Text.Contains(searchTerm)).Take(100).ProjectTo<SearchResponseItemDto>());
                        //}
                        //break;
                    case "body":
                        results.AddRange(new BodySearchSummaryController().Get(searchTerm).ToArray());
                        break;
                }
            }

            return results.AsQueryable();

        }
    }
}