﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;
using AutoMapper;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Application;

namespace FooDemoServer.Presentation.Controllers
{
    [ODataRoutePrefix("foodto")]
    public class FooDtoController : ODataControllerBase
    {

        static int counter = 0;




        public FooDtoController()
        {
            counter += 1;
        }


        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery]
        public IQueryable<FooDto> Get()
        {
            _dbConnection = new ProjectDbContext();

            _dbConnection.Database.Log = Console.Write;

            //using (var db = new ProjectDbContext())
            //{
            //    //var result = db.FooItems.Include(x=>x.Items).ToList<FooItem>();

            var result = _dbConnection.FooItems.ProjectTo<FooDto>();

                return result;
            //}
        }

        // GET api/values/5 
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery]
        public  FooDto Get([FromODataUri] Guid key)
        {
            using (var db = new ProjectDbContext())
            {
                return db.FooItems.ProjectTo<FooDto>().SingleOrDefault(x=>x.Id==key);
            }
        }

        // POST api/values 
        public void Post([FromBody]FooDto value)
        {
            _dbConnection.FooItems.Add(Mapper.Map<Foo>(value));
        }

        // PUT api/values/5 
        //public void Put(int id, [FromBody]Foo value)
        //{
        //}

        //// DELETE api/values/5 
        //public void Delete(int id)
        //{
        }


}