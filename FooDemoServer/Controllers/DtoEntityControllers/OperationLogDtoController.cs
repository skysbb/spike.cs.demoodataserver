﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;
using FooDemoServer.Shared.Models.DTOs.V1;

namespace FooDemoServer.Presentation.Controllers
{
    /// <summary>
    /// This Controller is the complex version of Odata, in that
    /// it has nested elements, which get passed through Automapper.
    /// At present, it's failing. No idea why. think it's due to Mapper...but not sure
    /// </summary>

        //[ODataRoutePrefix("body")]
    public class SessionOperationDtoController : ODataControllerBase<SessionOperation>
    {





        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        [EnableQuery(PageSize=100)]
        public IQueryable<SessionOperationDto> Get()
        {
            IQueryable <SessionOperationDto> results;
            results = 
                _dbSet
                //.Where(x => x.RecordState == RecordPersistenceState.Active )
                .ProjectTo<SessionOperationDto>()//x=>x.Names,x=>x.Channels,x=>x.Properties,x=>x.Claims
                ;

            return results;

        }

        //[ODataRoute("({key})")]
        public SessionOperationDto Get(Guid key)
        {
             return _dbSet.ProjectTo<SessionOperationDto>().SingleOrDefault(x=>x.Id==key);
        }

 

    }
}