﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;
using FooDemoServer.Shared.Models.DTOs.V1;

namespace FooDemoServer.Presentation.Controllers
{
    public class NotificationController : ODataControllerBase
    {



        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery(PageSize=100)]
        public IQueryable<NotificationDto> Get()
        {
            IQueryable <NotificationDto> results;
            results = _dbConnection.Notifications.Where(x => !x.Read).Take(100).ProjectTo<NotificationDto>();

            return results;

        }

        public NotificationDto Get(Guid key)
        {
             return _dbConnection.Notifications.ProjectTo<NotificationDto>().SingleOrDefault(x=>x.Id==key);
        }

        // POST api/values 
        public void Post([FromBody]NotificationDto value)
        {
            _dbConnection.Notifications.Add( Mapper.Map<Notification>(value));
        }

        //// PUT api/values/5 
        //public void Put(Guid key, [FromBody]NotificationDto value)
        //{
        //}


    }
}