﻿using FooDemoServer.Presentation.Controllers;
using FooDemoServer.Shared.Models.DTOs.V1;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData;

namespace FooDemoServer.Controllers
{
    public class SchoolSearchSummaryController : ODataControllerBase<School>
    {
        [EnableQuery(PageSize = 100)]
        public IQueryable<SearchResponseItemDto> Get(string text)
        {
            //IQueryable<SearchResponseItem> results;

            var tmp1 =
                _dbSet
                .Where(x =>
                x.RecordState == RecordPersistenceState.Active
                &&
                (x.Description.Contains(text))
                )
                .Include(x => x.Organisation)
                .Include(x => x.Principal)
                .ToArray();

            var tmp2 =
                tmp1
                .Select(x =>
                {
                    var title = (x.Organisation.Name);
                    var description = x.Description;
                    var breakText = "";

                   return  new SearchResponseItemDto {
                       SourceTypeKey = "school".ToLower(),
                       SourceIdentifier = x.Id.ToString(),
                       Title = title,
                       Description = description };
                }).AsQueryable();

            return tmp2;

        }

        private string MakeName(Body body)
        {
            string result = $"{body.Prefix} {body.GivenName} {body.MiddleNames} {body.SurName} {body.Suffix}".Trim();
            while (result.Contains("  "))
            {
                result = result.Replace("  ", " ");
            }
            return result;
        }
        private string MakeProtocol(BodyChannel bodyChannel)
        {
            string result;

            if (bodyChannel.Protocol != BodyChannelType.Postal)
            {
                result =  $"{bodyChannel.AddressStreetAndNumber}, {bodyChannel.AddressSuburb}, {bodyChannel.AddressCity}, {bodyChannel.AddressRegion}, {bodyChannel.AddressState}, {bodyChannel.AddressCountry}";
                while (result.Contains(", ,"))
                {
                    result = result.Replace(", , ", ", ");
                }
            }
            else
            {
                result = $"{bodyChannel.Protocol}: {bodyChannel.Address}";
            }
            return result;
        }

    }
}
