﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;
using FooDemoServer.Shared.Models.DTOs.V1;

namespace FooDemoServer.Presentation.Controllers
{
    public class MessageController : ODataControllerBase
    {
   

        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery(PageSize=100)]
        public IQueryable<MessageDto> Get()
        {
            IQueryable < MessageDto > results;
            results = _dbConnection.Messages.Where(x => !x.Read).Take(100).ProjectTo<MessageDto>();

            return results;

        }

        public MessageDto Get(Guid key)
        {
             return _dbConnection.Messages.ProjectTo<MessageDto>().SingleOrDefault(x=>x.Id==key);
        }

        // POST api/values 
        public void Post([FromBody]MessageDto value)
        {
            _dbConnection.Messages.Add( Mapper.Map<Message>(value));
        }

        //// PUT api/values/5 
        //public void Put(Guid key, [FromBody]MessageDto value)
        //{
        //}


    }
}