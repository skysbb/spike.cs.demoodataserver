﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Controllers.DtoEntityControllers.DTOs
{
    public class NotificationUpdateDto : IHasGuidId
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Status whether Message has been read.
        /// </summary>
        public bool Read { get; set; }


        /// <summary>
        /// The time the client read the message (if offline, will be different than time server records it).
        /// </summary>
        public DateTime DateTimeModifiedUtc { get; set; }

    }
}
