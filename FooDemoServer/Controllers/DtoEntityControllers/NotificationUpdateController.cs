﻿using FooDemoServer.Controllers.DtoEntityControllers.DTOs;
using FooDemoServer.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FooDemoServer.Controllers.DtoEntityControllers
{


    public class NotificationUpdateController : ODataControllerBase 
    {

        // POST api/values 
        public void Post([FromBody]NotificationUpdateDto value)
        {

            var record = _dbConnection.Notifications.SingleOrDefault(x => x.Id == value.Id);
            if (record == null)
            {
                return;
            }
            if (record.Read == value.Read)
            {
                // Idempotent.
                return;
            }
            record.Read = value.Read;
            record.DateTimeReadUtc = (record.Read)? value.DateTimeModifiedUtc:(DateTime?)null;
        }
    }
}
