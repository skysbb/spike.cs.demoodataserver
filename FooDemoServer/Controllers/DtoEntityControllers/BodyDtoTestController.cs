﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models.DTOs;
using System.Web.OData.Routing;
using System.Collections.ObjectModel;
using FooDemoServer.Shared.Models.DTOs.V1;

namespace FooDemoServer.Presentation.Controllers
{
    /// <summary>
    /// Controller created to see how Odata can return BodyDto objects (hooked together
    /// to all its sub elements such as Names, Properties, Claims) without 
    /// the Automapper stuff in between.... in order to track down why the other controller
    /// is failing.
    /// Notice that it works... (ie, without Automapper being in the picture)
    /// </summary>
    public class BodyDtoTest2Controller : ODataControllerBase
    {
    }

        //[ODataRoutePrefix("bodybasic")]
        public class BodyDtoTestController : ODataControllerBase
    {

        static ICollection<BodyDto> _cache = new Collection<BodyDto>();

        static BodyDtoTestController()
        {
            BodyDto body = new BodyDto
            {
                Id = 10.ToGuid(),
                RecordState = RecordPersistenceState.Active,
                Type = BodyType.Person
            };
            body.Category = new BodyCategoryDto { Id = 1.ToGuid(), RecordState = RecordPersistenceState.Active, Text = "Cat111" };

            body.Names.Add(new BodyAliasDto { Id = 1.ToGuid(), RecordState = RecordPersistenceState.Active, BodyFK = 10.ToGuid(), Name = "foo.bar", FirstName = "Foo", LastName = "Bar" });
            body.Properties.Add(new BodyPropertyDto { Id = 1.ToGuid(), RecordState = RecordPersistenceState.Active, BodyFK = 10.ToGuid(), Key = "PropA", Value = "Foo..." });
            body.Claims.Add(new BodyClaimDto { Id = 1.ToGuid(), RecordState = RecordPersistenceState.Active, BodyFK = 10.ToGuid(), Key = "ClaimA", Value = "Bar...", AuthorityKey = "MOE", Signature = "....................." });
            body.Channels.Add(new BodyChannelDto { Id = 1.ToGuid(), RecordState = RecordPersistenceState.Active, BodyFK = 10.ToGuid(), Protocol = BodyChannelType.Phone, Address = "123 foo@bar" });

            _cache.Add(body);
        }


        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute("")]
        [EnableQuery(PageSize=100)]
        public IQueryable<BodyDto> Get()
        {
            IQueryable <BodyDto> results;
            results = 
                _cache.AsQueryable().Where(x => x.RecordState == RecordPersistenceState.Active )
                .Include(x => x.Names)
                .Include(x=>x.Channels)
                .Include(x=>x.Properties)
                .Include(x=>x.Claims)
                ;

            return results;

        }

        //[ODataRoute("({key})")]
        public BodyDto Get(Guid key)
        {
             return _cache.AsQueryable().SingleOrDefault(x=>x.Id==key);
        }

    }
}