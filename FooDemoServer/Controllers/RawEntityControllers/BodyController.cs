﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using FooDemoServer.Shared.Models;
using System.Web.OData.Routing;

namespace FooDemoServer.Presentation.Controllers
{
    /// <summary>
    /// This Controller is the complex version of Odata, in that
    /// it has nested elements, which get passed through Automapper.
    /// At present, it's failing. No idea why.
    /// </summary>
    //[ODataRoutePrefix("body")]
    public class BodyController : ODataControllerBase
    {




        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        [EnableQuery(PageSize=100)]
        public IQueryable<Body> Get()
        {
            IQueryable <Body> results;
            results = 
                _dbConnection.Bodies.Where(x => x.RecordState == RecordPersistenceState.Active )
                .Include(x => x.Aliases)
                .Include(x=>x.Channels)
                .Include(x=>x.Properties)
                .Include(x=>x.Claims)
                ;

            return results;

        }



    }
}