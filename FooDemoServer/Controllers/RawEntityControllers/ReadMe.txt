﻿IMPORTANT:

These are API Controllers that are exposing 
actual CodeFirst Entities.

A basic security concept is to 
NEVER EVER Expose to 3rd parties APIs that 
Expose the shape of your DB.

It's also a maintenance/contractual issue --
you can't go break any 3rd party clients 
every time you change the database schema...

In other words -- these controllers ONLY exist
because this is a demo project, in which I'm 
showing the difference between Raw and Dto 
entity controllers.