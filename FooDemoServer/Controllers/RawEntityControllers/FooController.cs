﻿using FooDemoServer.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using FooDemoServer.Shared.Models.Entities;

namespace FooDemoServer.Presentation.Controllers
{

    public class FooController : ODataControllerBase
    {
        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        [EnableQuery]
        public IQueryable<Foo> Get()
        {
                var result = _dbConnection.FooItems.Include(x=>x.Items).ToList<Foo>();
                return result.AsQueryable();
        }

        // GET api/values/5 
        //[ApplyProxyDataContractResolverAttribute]
        public Foo Get(Guid id)
        {
                return _dbConnection.FooItems.SingleOrDefault(x=>x.Id==id);
        }

        //// POST api/values 
        //public void Post([FromBody]Foo value)
        //{
        //}

        //// PUT api/values/5 
        //public void Put(int id, [FromBody]Foo value)
        //{
        //}

        //// DELETE api/values/5 
        //public void Delete(int id)
        //{
        //}
    }
}