﻿This app is CodeFirst based.

The web.config's connection string is pointing to a localdb instance. If the db doesn't exist, it will create it.

If you make changes to the schema, the app will probably stop working until 
you get the Db to match the new shame, which you do by adding a migration:
Add-Migration [NameOfUpdate]
eg: Add-Migration Update01

And then applying the migration to the Db:
Update-Database

Invoking Update-Database ensures seeding occurs too.


## SSL

SSL over a Self-Hosting platform is a little bit of fun...cause most of us have totally
lost habits of working with certs while IIS was taking care of everything for us.

The logic is as follows:
* It's serving HTTP at 9000
* So HTTPS will have to be on another port (9001)
* The thumbprint of the self-signed dev cert is: ‎86f0156c388b8ffc75e999fcd590702cf55debf3 
* The appId is...dunno...anything will do. Eg: {00000000-0000-0000-0000-AABBCCDDEEFF}

# Get oversight first:
netsh http show urlacl

# Add a new http binding
netsh http add urlacl url=http://+:9000/ user=Everyone

# Add a new http(S) binding
(notice the s below...)
netsh http add urlacl url=https://+:9001/ user=Everyone

# In case you stuffed up
(in case you missed the s...as I did)
netsh http delete urlacl url=http://+:9001/

# Associate the cert to the binding:
netsh http add sslcert ipport=0.0.0.0:9001 certhash=86f0156c388b8ffc75e999fcd590702cf55debf3 appid={00000000-0000-0000-0000-AABBCCDDEEFF}

