namespace FooDemoServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class U1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bodies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Key = c.String(maxLength: 256),
                        Description = c.String(maxLength: 256),
                        Notes = c.String(maxLength: 2048),
                        CategoryFK = c.Guid(nullable: false),
                        Name = c.String(),
                        Prefix = c.String(),
                        GivenName = c.String(),
                        MiddleNames = c.String(),
                        SurName = c.String(),
                        Suffix = c.String(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BodyCategories", t => t.CategoryFK, cascadeDelete: true)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .Index(t => t.CategoryFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.BodyAlias",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        DisplayOrderHint = c.Int(nullable: false),
                        Name = c.String(maxLength: 256),
                        Prefix = c.String(maxLength: 50),
                        GivenName = c.String(maxLength: 100),
                        MiddleNames = c.String(maxLength: 100),
                        SurName = c.String(maxLength: 100),
                        Suffix = c.String(maxLength: 50),
                        Title = c.String(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Bodies", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.Tenants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        DisplayName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TenantClaims",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        Authority = c.String(nullable: false, maxLength: 50),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        AuthoritySignature = c.String(nullable: false, maxLength: 1024),
                        RecordState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "dbo.TenantProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        RecordState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "dbo.BodyCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Text = c.String(nullable: false, maxLength: 50),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.BodyChannels",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        DisplayOrderHint = c.Int(nullable: false),
                        Protocol = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50),
                        Address = c.String(maxLength: 256),
                        AddressStreetAndNumber = c.String(maxLength: 256),
                        AddressSuburb = c.String(maxLength: 100),
                        AddressCity = c.String(maxLength: 100),
                        AddressRegion = c.String(maxLength: 100),
                        AddressState = c.String(maxLength: 100),
                        AddressPostalCode = c.String(maxLength: 100),
                        AddressCountry = c.String(maxLength: 100),
                        AddressInstructions = c.String(maxLength: 100),
                        RecordState = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Bodies", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.BodyClaims",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        Authority = c.String(nullable: false, maxLength: 50),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        AuthoritySignature = c.String(nullable: false, maxLength: 1024),
                        RecordState = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Bodies", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.BodyProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        RecordState = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Bodies", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.Foos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Text = c.String(nullable: false),
                        Secret = c.String(nullable: false),
                        ImagePath = c.String(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.FooSubItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        ParentFK = c.Guid(nullable: false),
                        Text = c.String(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Foos", t => t.ParentFK, cascadeDelete: true)
                .Index(t => t.ParentFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Level = c.Int(nullable: false),
                        Read = c.Boolean(nullable: false),
                        DateTimeCreatedUtc = c.DateTime(nullable: false),
                        DateTimeReadUtc = c.DateTime(),
                        Text = c.String(),
                        RecordState = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                        Tenant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.Tenant_Id)
                .Index(t => t.Tenant_Id);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        From = c.String(),
                        ImageUrl = c.String(),
                        Class = c.String(),
                        DateTimeCreatedUtc = c.DateTime(nullable: false),
                        DateTimeReadUtc = c.DateTime(),
                        Read = c.Boolean(nullable: false),
                        Value = c.Int(nullable: false),
                        Text = c.String(),
                        RecordState = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                        Tenant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.Tenant_Id)
                .Index(t => t.Tenant_Id);
            
            CreateTable(
                "dbo.Spike01Item",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsOrg = c.Boolean(nullable: false),
                        OrgName = c.String(),
                        First = c.String(),
                        Middles = c.String(),
                        Last = c.String(),
                        Description = c.String(),
                        Notes = c.String(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.Spike01SubItem",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentFK = c.Guid(nullable: false),
                        DisplayOrderHint = c.Int(nullable: false),
                        Protocol = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Address = c.String(),
                        Street = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Spike01Item", t => t.ParentFK, cascadeDelete: true)
                .Index(t => t.ParentFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.UserProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        RecordState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        Authority = c.String(nullable: false, maxLength: 50),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        AuthoritySignature = c.String(nullable: false, maxLength: 1024),
                        RecordState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sessions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        StartDateTimeUtc = c.DateTime(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                        UserFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserFK, cascadeDelete: true)
                .Index(t => t.TenantFK)
                .Index(t => t.UserFK);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Ref = c.String(nullable: false),
                        BodyFK = c.Guid(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bodies", t => t.BodyFK)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .Index(t => t.BodyFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Ref = c.String(nullable: false, maxLength: 50),
                        CustomerFK = c.Guid(),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Customers", t => t.CustomerFK)
                .Index(t => t.CustomerFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.OrderLineItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Text = c.String(nullable: false, maxLength: 50),
                        OrderFK = c.Guid(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .ForeignKey("dbo.Orders", t => t.OrderFK, cascadeDelete: true)
                .Index(t => t.OrderFK)
                .Index(t => t.TenantFK);
            
            CreateTable(
                "dbo.Schools",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentFK = c.Guid(),
                        Type = c.Int(nullable: false),
                        Key = c.String(),
                        Description = c.String(),
                        OrganisationFK = c.Guid(nullable: false),
                        PrincipalFK = c.Guid(nullable: false),
                        Note = c.String(),
                        RecordState = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bodies", t => t.OrganisationFK)
                .ForeignKey("dbo.Bodies", t => t.PrincipalFK)
                .ForeignKey("dbo.Tenants", t => t.TenantFK)
                .Index(t => t.OrganisationFK)
                .Index(t => t.PrincipalFK)
                .Index(t => t.TenantFK);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schools", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Schools", "PrincipalFK", "dbo.Bodies");
            DropForeignKey("dbo.Schools", "OrganisationFK", "dbo.Bodies");
            DropForeignKey("dbo.Customers", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Orders", "CustomerFK", "dbo.Customers");
            DropForeignKey("dbo.Orders", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.OrderLineItems", "OrderFK", "dbo.Orders");
            DropForeignKey("dbo.OrderLineItems", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Customers", "BodyFK", "dbo.Bodies");
            DropForeignKey("dbo.Sessions", "UserFK", "dbo.Users");
            DropForeignKey("dbo.Sessions", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.UserProperties", "OwnerFK", "dbo.Users");
            DropForeignKey("dbo.UserClaims", "OwnerFK", "dbo.Users");
            DropForeignKey("dbo.Spike01Item", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Spike01SubItem", "ParentFK", "dbo.Spike01Item");
            DropForeignKey("dbo.Spike01SubItem", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Notifications", "Tenant_Id", "dbo.Tenants");
            DropForeignKey("dbo.Messages", "Tenant_Id", "dbo.Tenants");
            DropForeignKey("dbo.Foos", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.FooSubItems", "ParentFK", "dbo.Foos");
            DropForeignKey("dbo.FooSubItems", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Bodies", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.BodyProperties", "OwnerFK", "dbo.Bodies");
            DropForeignKey("dbo.BodyProperties", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.BodyClaims", "OwnerFK", "dbo.Bodies");
            DropForeignKey("dbo.BodyClaims", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.BodyChannels", "OwnerFK", "dbo.Bodies");
            DropForeignKey("dbo.BodyChannels", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.Bodies", "CategoryFK", "dbo.BodyCategories");
            DropForeignKey("dbo.BodyCategories", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.BodyAlias", "OwnerFK", "dbo.Bodies");
            DropForeignKey("dbo.BodyAlias", "TenantFK", "dbo.Tenants");
            DropForeignKey("dbo.TenantProperties", "OwnerFK", "dbo.Tenants");
            DropForeignKey("dbo.TenantClaims", "OwnerFK", "dbo.Tenants");
            DropIndex("dbo.Schools", new[] { "TenantFK" });
            DropIndex("dbo.Schools", new[] { "PrincipalFK" });
            DropIndex("dbo.Schools", new[] { "OrganisationFK" });
            DropIndex("dbo.OrderLineItems", new[] { "TenantFK" });
            DropIndex("dbo.OrderLineItems", new[] { "OrderFK" });
            DropIndex("dbo.Orders", new[] { "TenantFK" });
            DropIndex("dbo.Orders", new[] { "CustomerFK" });
            DropIndex("dbo.Customers", new[] { "TenantFK" });
            DropIndex("dbo.Customers", new[] { "BodyFK" });
            DropIndex("dbo.Sessions", new[] { "UserFK" });
            DropIndex("dbo.Sessions", new[] { "TenantFK" });
            DropIndex("dbo.UserClaims", new[] { "OwnerFK" });
            DropIndex("dbo.UserProperties", new[] { "OwnerFK" });
            DropIndex("dbo.Spike01SubItem", new[] { "TenantFK" });
            DropIndex("dbo.Spike01SubItem", new[] { "ParentFK" });
            DropIndex("dbo.Spike01Item", new[] { "TenantFK" });
            DropIndex("dbo.Notifications", new[] { "Tenant_Id" });
            DropIndex("dbo.Messages", new[] { "Tenant_Id" });
            DropIndex("dbo.FooSubItems", new[] { "TenantFK" });
            DropIndex("dbo.FooSubItems", new[] { "ParentFK" });
            DropIndex("dbo.Foos", new[] { "TenantFK" });
            DropIndex("dbo.BodyProperties", new[] { "TenantFK" });
            DropIndex("dbo.BodyProperties", new[] { "OwnerFK" });
            DropIndex("dbo.BodyClaims", new[] { "TenantFK" });
            DropIndex("dbo.BodyClaims", new[] { "OwnerFK" });
            DropIndex("dbo.BodyChannels", new[] { "TenantFK" });
            DropIndex("dbo.BodyChannels", new[] { "OwnerFK" });
            DropIndex("dbo.BodyCategories", new[] { "TenantFK" });
            DropIndex("dbo.TenantProperties", new[] { "OwnerFK" });
            DropIndex("dbo.TenantClaims", new[] { "OwnerFK" });
            DropIndex("dbo.BodyAlias", new[] { "TenantFK" });
            DropIndex("dbo.BodyAlias", new[] { "OwnerFK" });
            DropIndex("dbo.Bodies", new[] { "TenantFK" });
            DropIndex("dbo.Bodies", new[] { "CategoryFK" });
            DropTable("dbo.Schools");
            DropTable("dbo.OrderLineItems");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
            DropTable("dbo.Sessions");
            DropTable("dbo.Users");
            DropTable("dbo.UserClaims");
            DropTable("dbo.UserProperties");
            DropTable("dbo.Spike01SubItem");
            DropTable("dbo.Spike01Item");
            DropTable("dbo.Notifications");
            DropTable("dbo.Messages");
            DropTable("dbo.FooSubItems");
            DropTable("dbo.Foos");
            DropTable("dbo.BodyProperties");
            DropTable("dbo.BodyClaims");
            DropTable("dbo.BodyChannels");
            DropTable("dbo.BodyCategories");
            DropTable("dbo.TenantProperties");
            DropTable("dbo.TenantClaims");
            DropTable("dbo.Tenants");
            DropTable("dbo.BodyAlias");
            DropTable("dbo.Bodies");
        }
    }
}
