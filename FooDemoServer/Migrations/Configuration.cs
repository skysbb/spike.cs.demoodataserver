namespace FooDemoServer.Migrations
{
    using FooDemoServer.Application;
    using FooDemoServer.DbContextSeeder;
    using FooDemoServer.Shared.Models.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjectDbContext >
    {
        public Configuration()
        {
            //Needed to setup for var used in app.config.
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(path, "App_Data"));


            AutomaticMigrationsEnabled = false;
        }
        

        protected override void Seed(ProjectDbContext context)
        {
            ProjectDbContextSeeder.Seed(context);
        }
    }
}
