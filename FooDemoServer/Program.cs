﻿using FooDemoServer.Application;
using FooDemoServer.Migrations;
using FooDemoServer.Presentation.Mappers;
using FooDemoServer.Services;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer
{
    class Program
    {
        static void Main(string[] args)
        {

            SchoolCsvImporterService source = new SchoolCsvImporterService();
            var test = source.Import().ToArray();

            //Needed to setup for var used in app.config.
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(path, "App_Data"));

            new AutoMapperConfiguration().Initialize();


            //string[] appPath = path.Split(new string[] { "bin\\" }, StringSplitOptions.None);
            //AppDomain.CurrentDomain.SetData("DataDirectory", appPath[0]);

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ProjectDbContext, Configuration>());

            // FOR SSL, read this:
            // https://frendsrnd.wordpress.com/2014/02/03/httpselfhostserver-hosted-web-api-with-https-and-windows-authentication-enabled/
            // but also this (which simplifies stuff):
            // https://stackoverflow.com/questions/24976425/running-self-hosted-owin-web-api-under-non-admin-account
            string baseAddress = "https://localhost:9001/";

            // Start OWIN host 
            using (WebApp.Start<Startup>(url: baseAddress))
            {

                
                // Create HttpCient and make a request to api/values 
                HttpClient client = new HttpClient();

                var response = client.GetAsync(baseAddress + "api/classic/foo").Result;

                while (true)
                {
                    Console.WriteLine("Web Server is running.");
                    Console.WriteLine("Root:" + baseAddress);
                    Console.WriteLine("");
                    Console.WriteLine("Purpose:");
                    Console.WriteLine("=================");
                    Console.WriteLine("The purpose of this server is provide apis to develop front end spikes/pocs.");
                    Console.WriteLine("It's certainly *not* a real app grade.");
                    Console.WriteLine("Features:");
                    Console.WriteLine("* Self Seeding CodeFirst database.");
                    Console.WriteLine("* ODATA Endpoints, demonstrating flexibility.");
                    Console.WriteLine("* Some traditional API endpoints.");
                    Console.WriteLine("");
                    Console.WriteLine("Example Routes:" );
                    Console.WriteLine("=================");
                    Console.WriteLine("");
                    Console.WriteLine("Retrive ODATA Metadata (ie, ODATA equivalent to SOAP WDSL):");
                    Console.WriteLine("-----------------------------------------------------------");
                    Console.WriteLine(" " + baseAddress + "api/$metadata");
                    Console.WriteLine("");
                    Console.WriteLine("Return DB Entities (in prod, don't do this!):");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine(" " + baseAddress + "api/body");
                    Console.WriteLine(" " + baseAddress + "api/body?$expand=Names,Channels,Properties,Claims");
                    Console.WriteLine("");
                    Console.WriteLine("Return Securable and versionable DTOs instead:");
                    Console.WriteLine("----------------------------------------------");
                    Console.WriteLine(" " + baseAddress + "api/bodydto");
                    Console.WriteLine(" " + baseAddress + "api/bodydto?$expand=Names");
                    Console.WriteLine(" " + baseAddress + "api/bodydto?$expand=Names,Properties,Claims");
                    Console.WriteLine(" " + baseAddress + "api/bodydto?$expand=Names,Channels,Properties,Claims");
                    Console.WriteLine(" " + baseAddress + "api/bodydto?$expand=Names,Category,Properties,Claims&$filter=CategoryFK%20eq%2000000001-0000-0000-0000-000000000000");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    Console.WriteLine("For [Role] based data column manipulation example:");
                    Console.WriteLine("--------------------------------------------------");
                    Console.WriteLine(" " + baseAddress + "api/fooitemdto?$expand=Items");
                    Console.WriteLine("");
                    Console.WriteLine("SubItem Queries examples (nice, but shitty syntax):");
                    Console.WriteLine("--------------------------------------------------");
                    Console.WriteLine(" " + baseAddress + "api/bodydto?$expand=Names&$filter=Names/any(o:%20o/FirstName%20eq%20%27Betty%27)");
                    Console.WriteLine(" " + baseAddress + "api/bodydto?$expand=Names,Channels,Properties,Claims&$filter=Names/any(o:%20o/FirstName%20eq%20%27Betty%27)");
                    Console.WriteLine("");
                    Console.WriteLine("Non-OData calls (NOT WORKING):");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine(" " + baseAddress + "api/legacy/values");
                    Console.WriteLine(" " + baseAddress + "api/legacy/spike01itemsummary");
                    Console.WriteLine(" " + baseAddress + "api/legacy/spike01subitemsummary");
                    Console.WriteLine(" " + baseAddress + "api/legacy/spike01subitemorderupdate");
                    Console.WriteLine("");
                    Console.WriteLine("Queries for Contracted Spike 1:");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("This is to create a demo workflow as per http://skysigal.com/diagrams/bread/");
                    Console.WriteLine(" " + baseAddress + "api/spike01item");
                    Console.WriteLine(" " + baseAddress + "api/spike01subitem");
                    Console.WriteLine(" " + baseAddress + "api/spike01item?$expand=Items");
                    Console.WriteLine(" " + baseAddress + "api/spike01item?$expand=Items&$top=10&$skip=60");
                    Console.WriteLine(" " + baseAddress + "api/spike01item(00000003-0000-0000-0000-000000000000)");
                    Console.WriteLine(" " + baseAddress + "api/subitemorderupdate");


                    Console.WriteLine("");
                    Console.WriteLine("type 'exit' to quit");
                    string line = Console.ReadLine(); // Get string from user

                    if (line == "exit") // Check string
                    {
                        break;
                    }
                }
            }
        }
    }


}
