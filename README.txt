This app is CodeFirst based.

The web.config's connection string is pointing to a localdb instance. If the db doesn't exist, it will create it.

If you make changes to the schema, the app will probably stop working until 
you get the Db to match the new shame, which you do by adding a migration:
Add-Migration [NameOfUpdate]
eg: Add-Migration Update01

And then applying the migration to the Db:
Update-Database

Invoking Update-Database ensures seeding occurs too.