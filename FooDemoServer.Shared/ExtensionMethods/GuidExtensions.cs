﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer
{
    public static class GuidExtensions
    {
        public static Guid Generate(this Guid guid)
        {
            return Guid.NewGuid();
        }
    }
}
