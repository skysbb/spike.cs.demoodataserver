﻿using System;

namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasUserFK
    {
        Guid UserFK { get; set; }
    }
}
