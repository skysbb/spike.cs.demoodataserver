﻿namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasTitle {
            string Title { get; set; }
    }

}
