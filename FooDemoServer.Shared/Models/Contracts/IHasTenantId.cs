﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasTenantFK
    {
        Guid TenantFK { get; set; }
    }
}
