﻿namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasEnabled
    {
        bool Enabled { get; set; }
    }
}
