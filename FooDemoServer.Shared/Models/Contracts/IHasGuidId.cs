﻿using System;


namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasGuidId :IHasId<Guid>
    {
    }

}
