﻿namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasId<T> {
        T Id { get; set; }
    }
}
