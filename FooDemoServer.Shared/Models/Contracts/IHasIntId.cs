﻿namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasIntId : IHasId<int>
    {

    }
}
