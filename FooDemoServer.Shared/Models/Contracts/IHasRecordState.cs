﻿using FooDemoServer.Shared.Models.Entities;


namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasRecordState
    {
        RecordPersistenceState RecordState { get; set; }
    }
}
