﻿using System;


namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasOwnerFK {
        Guid OwnerFK { get; set; }
    }

}
