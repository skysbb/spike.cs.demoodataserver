﻿using System;

namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasDateTimeCreatedUtc
    {
        DateTime DateTimeCreatedUtc {get;set;}
    }
}
