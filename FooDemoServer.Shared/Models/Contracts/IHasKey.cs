﻿namespace FooDemoServer.Shared.Models.Contracts
{
    public interface IHasKey
    {
        string Key { get; set; }
    }

}
