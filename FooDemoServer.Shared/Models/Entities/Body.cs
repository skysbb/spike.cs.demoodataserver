﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{
    // Should be wary of enums, but this will do for a demo
    public enum BodyType
    {
        // An error state.
        Undefined = 0,
        Person = 1,
        // Enterprise, Company, Group
        Organisation = 2,
    }

    public enum BodyChannelType
    {
        // An error state.
        Undefined,
        Phone,
        Email,
        Twitter,
        Postal
        // etc.
    }

    public class Body : TenantStateEntityBase
    {

        public BodyType Type { get; set; }

        /// <summary>
        /// Optional unique Key.
        /// </summary>
        public string Key { get; set; }

        public Guid CategoryFK { get; set; }
        public BodyCategory Category { get; set; }

        // Display if Type=Organisation
        public string Name
        {
            get { return _name; }
            set { /* TODO: this is where auto parsing would go if Type=Person */ _name = value; }
        }
        private string _name;

        public string Prefix { get; set; }
        public string GivenName { get; set; }
        public string MiddleNames { get; set; }
        public string SurName { get; set; }
        public string Suffix { get; set; }

        public ICollection<BodyAlias> Aliases { get { if (_aliases == null) { _aliases = new Collection<BodyAlias>(); } return _aliases; } }
        ICollection<BodyAlias> _aliases;

        // A Body can be contacted by many different types of channels:
        public ICollection<BodyChannel> Channels { get { if (_channels == null) { _channels = new Collection<BodyChannel>(); } return _channels; } }
        ICollection<BodyChannel> _channels;

        public ICollection<BodyProperty> Properties { get { if (_properties == null) { _properties = new Collection<BodyProperty>(); } return _properties; } set { _properties = value; } }
        ICollection<BodyProperty> _properties;

        public ICollection<BodyClaim> Claims { get { if (_claims == null) { _claims = new Collection<BodyClaim>(); } return _claims; } set { _claims = value; } }
        ICollection<BodyClaim> _claims;


        public string Description { get; set; }
        public string Notes { get; set; }

    }
}
