﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class CourseEnrollment : TenantStateEntityBase
    {

        public Student StudentFK { get; set; }
        public Student Sudent { get; set; }

        public Guid CourseFK { get; set; }
        public Course Course { get; set; }
    }
}