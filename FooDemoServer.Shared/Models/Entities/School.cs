﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{
    public enum SchoolEstablishmentType {
        Undefined =0,
        School = 2,
        Group = 2,
        COL = 4,
    }

    public class School : TenantStateEntityBase
    {

        public Guid? ParentFK { get; set; }

        public SchoolEstablishmentType Type { get; set; }

        public string Key { get; set; }
        public string Description { get; set; }

        public Guid OrganisationFK {get;set;}
        public Body Organisation { get; set; }

        //FK to Principal Body
        public Guid PrincipalFK { get; set; }
        public Body Principal { get; set; }
        

        public string Note { get; set; }
 
    }
}
