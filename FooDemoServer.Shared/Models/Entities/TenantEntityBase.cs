﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{
    public abstract class GuidIdEntityBase : IHasGuidId {
        public Guid Id { get; set; }

        public GuidIdEntityBase()
        {
            Id = Guid.NewGuid();
        }
    }

    public abstract class TenantEntityBase : GuidIdEntityBase, IHasTenantFK
    {

        public Tenant Tenant { get; set; }
        public Guid TenantFK { get; set; }

    }
    public abstract class TenantStateEntityBase : TenantEntityBase, IHasRecordState
    {
        public RecordPersistenceState RecordState { get; set; }

    }
}
