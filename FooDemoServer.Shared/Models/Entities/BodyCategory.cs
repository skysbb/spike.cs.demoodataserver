﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{

    public class BodyCategory : TenantStateEntityBase
    {
        public virtual string Text { get; set; }

    }
}
