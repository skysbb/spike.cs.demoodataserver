﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{

    public class Message : TenantStateEntityBase, IHasDateTimeCreatedUtc
    {
        public TraceLevel Level {get; set;}
        public bool Read { get; set; }
        public DateTime DateTimeCreatedUtc { get; set; }
        public DateTime? DateTimeReadUtc { get; set; }
        public string Text {get;set;}
    }
}
