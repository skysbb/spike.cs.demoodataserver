﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    //[DataContract(IsReference = true)]
    public class FooSubItem : TenantStateEntityBase
    {

        public string Text { get; set; }

        public Guid? ParentFK { get; set; }

        //public virtual Foo Parent { get { return _fooItem; } set { _fooItem = value; } }
        Foo _fooItem;
    }
}
