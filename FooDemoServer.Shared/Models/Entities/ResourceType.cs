﻿namespace FooDemoServer.Shared.Models.Entities
{
    public enum ResourceType {
        Undefined =0,
        Location = 1,
        Device = 2,
        Service = 3,
        Time = 4,
    }
}
