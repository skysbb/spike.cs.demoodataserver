﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class BodyAlias : TenantStateEntityBase, IHasOwnerFK, IHasDisplayOrderHint, IHasTitle
    {
        public Guid OwnerFK { get; set; }

        public int DisplayOrderHint { get; set; }

        //Label to identify it (Alias, etc.)
        public string Title { get; set; }

        // Display if Type=Organisation
        public string Name
        {
            get { return _name; }
            set { /* TODO: this is where auto parsing would go if Type=Person */ _name = value; }
        }
        private string _name;

        public string Prefix { get; set; }
        public string GivenName { get; set; }
        public string MiddleNames { get; set; }
        public string SurName { get; set; }
        public string Suffix { get; set; }
    }
}
