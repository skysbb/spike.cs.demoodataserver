﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class Session : GuidIdEntityBase, IHasGuidId, IHasEnabled, IHasUserFK
    {

        public bool Enabled { get; set; }
        public DateTime StartDateTimeUtc { get; set; }

        public Tenant Tenant { get; set; }
        public Guid TenantFK { get; set; }

        public Guid UserFK { get; set; }
        public User User { get; set; }



    }
}