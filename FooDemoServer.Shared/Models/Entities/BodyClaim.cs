﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class BodyClaim : TenantStateEntityBase, IHasRecordState, IHasOwnerFK
    {
        public Guid OwnerFK { get; set; }
        public string Authority { get; set; }
        public virtual string AuthoritySignature { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }


}
