﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{
    public enum NotificationType {
        Undefined,
        Notification,
        Message,
        Task
    }

    public class Notification : TenantStateEntityBase
    {
        public NotificationType Type { get; set; }

        public TraceLevel Level { get; set; }

        public string From { get; set; }

        public string ImageUrl { get; set; }
        
        public string Class { get; set; }

        public DateTime DateTimeCreatedUtc { get; set; }
        public DateTime? DateTimeReadUtc { get; set; }

        /// <summary>
        /// Status whether Message has been read.
        /// </summary>
        public bool Read { get; set; }

        /// <summary>
        /// 1-100% complete.
        /// </summary>
        public int Value { get; set; }

        public string Text { get; set; }


    }
}
