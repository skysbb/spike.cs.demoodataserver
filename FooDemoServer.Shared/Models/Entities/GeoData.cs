using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    
    public class GeoData : TenantStateEntityBase
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal? Value { get; set; }
        public string Color { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Draggable { get; set; }


    }
}