﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{

    // A customer is associated to a Person (required).
    public class Customer : TenantStateEntityBase
    {
        public virtual string Ref { get; set; }
        public virtual Guid? BodyFK { get; set; }
        public virtual Body Body { get; set; }

        public virtual IList<Order> Orders { get; set; } // navigation property, entity type
    }
}
