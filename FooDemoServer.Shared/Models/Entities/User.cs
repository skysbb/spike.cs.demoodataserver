﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FooDemoServer.Shared.Models.Entities
{
    public class User : GuidIdEntityBase, IHasEnabled
    {


        public bool Enabled { get; set; }

        // The Application DisplayName
        public string DisplayName { get; set; }

        public ICollection<UserProperty> Properties { get { if (_properties == null) { _properties = new Collection<UserProperty>(); } return _properties; } set { _properties = value; } }
        ICollection<UserProperty> _properties;

        public ICollection<UserClaim> Claims { get { if (_claims == null) { _claims = new Collection<UserClaim>(); } return _claims; } set { _claims = value; } }
        ICollection<UserClaim> _claims;


    }
}
