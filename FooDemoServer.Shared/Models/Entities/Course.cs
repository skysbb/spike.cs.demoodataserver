﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.Entities
{

    public class Course : TenantStateEntityBase, IHasRecordState
    {


        public string Key { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public DateTimeOffset AvailableStartTimeUtc { get; set; }
        public DateTimeOffset AvailableEndTimeUtc { get; set; }

        public string ChronSchedule { get; set; }

        public TimeSpan Duration { get; set; }


    }
}