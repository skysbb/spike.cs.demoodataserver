﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;

namespace FooDemoServer.Shared.Models.Entities
{
    public class Order : TenantStateEntityBase
    {
        public virtual string Ref { get; set; } // structural property, primitive type
        public virtual Guid? CustomerFK {get;set;}
        public Customer Customer { get; set; }

        public virtual IList<OrderLineItem> LineItems { get; set; } // navigation property, entity type
    }
}
