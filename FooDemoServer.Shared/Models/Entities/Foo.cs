﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FooDemoServer.Shared.Models.Contracts;

namespace FooDemoServer.Shared.Models.Entities
{
    //[DataContract(IsReference = true)]
    public class Foo : TenantStateEntityBase
    {

        public string Text { get; set; }

        public string Secret { get; set; }

        public string ImagePath { get; set; }

        public virtual ICollection<FooSubItem> Items { get { if (_items == null) { _items = new Collection<FooSubItem>(); } return _items; } set { _items = value; } }
        ICollection<FooSubItem> _items;
    }
}
