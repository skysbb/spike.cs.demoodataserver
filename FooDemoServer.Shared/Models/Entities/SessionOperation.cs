﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class SessionOperation : GuidIdEntityBase, IHasKey {

        public Guid SessionFK { get; set; }
        public Session Session {get;set;}

        public string ClientIP { get; set; }
        public string Key { get; set; }
        public string Notes { get; set; }
    }

}
