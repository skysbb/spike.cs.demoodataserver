﻿namespace FooDemoServer.Shared.Models.Entities
{
    public enum TraceLevel {
        Critical,
        Error,
        Warn,
        Info,
        Debug,
        Verbose
    }
}
