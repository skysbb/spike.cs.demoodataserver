﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class Spike01SubItem : TenantEntityBase, IHasDisplayOrderHint 
    {
        // It's just some record -- but is large enough that it should not be
        // rendered as a single row in a grid, but instead be a mini form.
        // Added extra/useless fields to ensure that when rendering, that it's too big
        // to make mistake of rendering as one line...this should be more -- a single record,
        // under the parent record

        public Guid ParentFK { get; set; }

        //Order in which SubItems are displayed
        public int DisplayOrderHint { get; set; }

        //Custom Title to use for Protocol (eg: 'Home Phone')
        public string Title { get; set; }

        //Communication protocol:
        public Protocol Protocol { get; set; }

        //If Postal, show Street, City, Country -- else show only Address:
        public string Address { get; set; }

        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }




    }

}
