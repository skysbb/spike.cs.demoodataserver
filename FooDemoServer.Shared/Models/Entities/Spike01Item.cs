﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FooDemoServer.Shared.Models.Entities
{
    public class Spike01Item : TenantEntityBase
    {

        public bool IsOrg {get;set;}
        //If IsOrg, show only this field:
        public string OrgName { get; set; }

        //Is IsOrg=false (ie, Person), show this information:
        public string First { get; set; }
        public string Middles { get; set; }
        public string Last { get; set; }
        //Optional note about person/org
        public string Description { get; set; }
        // just to ensure form take more than one line.
        public string Notes { get; set; }
        //This is the list of ways to get in touch with the above person/org:
        public ICollection<Spike01SubItem> Items {get { if (_subItems == null){ _subItems = new Collection<Spike01SubItem>(); }return _subItems; } }
        private ICollection<Spike01SubItem> _subItems;



    }

    //Communication Protocol
    public enum Protocol {
        Phone,
        Email,
        Postal
    }

}
