﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.Entities
{
    public class OrderLineItem : TenantStateEntityBase
    {
        public virtual Guid OrderFK { get; set; }
        public virtual string Text { get; set; } // structural property, primitive type
    }
}
