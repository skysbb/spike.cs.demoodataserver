﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;


namespace FooDemoServer.Shared.Models.Entities
{
    public class Student : TenantStateEntityBase
    {

        //CMS info on student:
        public Guid BodyFK { get; set; }
        public Body Body { get; set; }

        public School PreferredSchool { get; set; }
        public ICollection<School> Schools { get; set; }

    }
}
