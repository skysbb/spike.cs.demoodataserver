﻿using FooDemoServer.Shared.Models.Contracts;
using System;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class SessionDto : IHasGuidId, IHasEnabled, IHasUserFK, IHasTenantFK
    {
        public Guid Id { get; set; }

        public bool Enabled { get; set; }
        public DateTime StartDateTimeUtc { get; set; }
        public Guid UserFK { get; set; }
        public Guid TenantFK { get; set; }
    }

}
