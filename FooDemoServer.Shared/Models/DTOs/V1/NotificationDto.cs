﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class NotificationDto : IHasGuidId, IHasTenantFK //, IHasRecordState
    {
        public NotificationDto() { Id = Guid.NewGuid(); }

        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }

        public NotificationType Type { get; set; }

        public TraceLevel Level { get; set; }

        public string From { get; set; }

        public string ImageUrl { get; set; }

        public string Class { get; set; }

        public DateTime DateTimeCreatedUtc { get; set; }
        public DateTime? DateTimeReadUtc { get; set; }

        /// <summary>
        /// Status whether Message has been read.
        /// </summary>
        public bool Read { get; set; }

        /// <summary>
        /// 1-100% complete.
        /// </summary>
        public int Value { get; set; }

        public string Text { get; set; }


    }

}
