﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class BodyChannelDto : IHasGuidId, IHasTenantFK, IHasRecordState, IHasDisplayOrderHint
    {

        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }
        public Guid BodyFK { get; set; }
        public virtual RecordPersistenceState RecordState { get; set; }
        public int DisplayOrderHint { get; set; }


        // Protocol: Phone, Email, Twitter, Instagram
        public BodyChannelType Protocol { get; set; }
        
        // Home Address, Home Phone, Office Address, Office Email, etc.
        public string Title { get; set; }

        // Email Address, etc. -- all except postal.
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {    // TODO: This is where auto parsing of address (if Postal, only) should go.
                _address = value;
            }
        }
        string _address;
        // Display these fields only if Protocol is Postal. Otherwise, Address is sufficient for all electronic protocols.
        public string AddressStreetAndNumber { get; set; }
        public string AddressSuburb { get; set; }
        public string AddressCity { get; set; }
        public string AddressRegion { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        // Amazes me that in two centuries, Postal Codes have not progressed to include encoding of CountryId + PostalCode +  StreetId + StreetPosition + BlockId + UnitId
        public string AddressPostalCode { get; set; }
        public string AddressInstructions { get; set; }
    }
}
