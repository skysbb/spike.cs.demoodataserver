﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class SessionOperationDto : IHasGuidId, IHasKey
    {

        public Guid Id { get; set; }
        public Guid SessionFK { get; set; }

        public string ClientIP { get; set; }
        public string Key { get; set; }
        public string Notes { get; set; }
    }


}
