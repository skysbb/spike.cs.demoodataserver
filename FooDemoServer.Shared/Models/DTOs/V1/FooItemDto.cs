﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{


    public class FooDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }


        public virtual RecordPersistenceState RecordState { get; set; }

        public string Description { get; set; }

        public string TopSecret { get; set; }

        public string ImagePath { get; set; }

        public virtual ICollection<FooSubItemDto> Items { get { if (_items == null) { _items = new Collection<FooSubItemDto>(); } return _items; } set { _items = value; } }
        ICollection<FooSubItemDto> _items;
    }

}
