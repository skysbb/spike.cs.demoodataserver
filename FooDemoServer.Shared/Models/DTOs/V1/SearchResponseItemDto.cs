﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class SearchResponseItemDto //: IHasGuidId, IHasRecordState
    {
        
        public string SourceTypeKey { get; set; }
        [Key]
        public string SourceIdentifier { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
    }
}
