﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class BodyPropertyDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }
        public Guid BodyFK { get; set; }

        public virtual RecordPersistenceState RecordState { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
