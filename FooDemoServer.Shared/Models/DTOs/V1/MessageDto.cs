﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class MessageDto : IHasGuidId, IHasTenantFK, IHasDateTimeCreatedUtc
    {
        
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }

        public TraceLevel Level { get; set; }
        public bool Read { get; set; }
        public string Text { get; set; }
        public DateTime DateTimeCreatedUtc { get; set; }
        public DateTime DateTimeReadUtc { get; set; }
    }
}
