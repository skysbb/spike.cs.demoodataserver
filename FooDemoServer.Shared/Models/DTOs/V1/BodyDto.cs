﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{

    public class BodyDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }

        public RecordPersistenceState RecordState { get; set; }

        public BodyType Type { get; set; }

        public string Key { get; set; }

        public Guid CategoryFK { get; set; }
        public BodyCategoryDto Category { get; set; }

        //public Guid PreferredNameFK { get; set; }
        //public BodyNameDto PreferredName { get; set; }

        // Display if Type=Organisation
        public string Name
        {
            get { return _name; }
            set { /* TODO: this is where auto parsing would go if Type=Person */ _name = value; }
        }
        private string _name;

        public string Prefix { get; set; }
        public string GivenName { get; set; }
        public string MiddleNames { get; set; }
        public string SurName { get; set; }
        public string Suffix { get; set; }


        public ICollection<BodyAliasDto> Names { get { if (_names == null) { _names = new Collection<BodyAliasDto>(); } return _names; } set { _names = value; } }
        ICollection<BodyAliasDto> _names;

        public ICollection<BodyChannelDto> Channels { get { if (_channels == null) { _channels = new Collection<BodyChannelDto>(); } return _channels; } set { _channels = value; } }
        ICollection<BodyChannelDto> _channels;

        public ICollection<BodyPropertyDto> Properties { get { if (_properties == null) { _properties = new Collection<BodyPropertyDto>(); } return _properties; } set { _properties = value; } }
        ICollection<BodyPropertyDto> _properties;

        public ICollection<BodyClaimDto> Claims { get { if (_claims == null) { _claims = new Collection<BodyClaimDto>(); } return _claims; } set { _claims = value; } }
        ICollection<BodyClaimDto> _claims;
    }
}
