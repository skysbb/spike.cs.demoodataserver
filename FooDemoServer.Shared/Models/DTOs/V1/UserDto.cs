﻿using FooDemoServer.Shared.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class UserDto : IHasGuidId, IHasEnabled
    {
        public Guid Id { get; set; }

        public bool Enabled { get; set; }
        public string DisplayName { get; set; }

        public ICollection<UserPropertyDto> Properties { get { if (_properties == null) { _properties = new Collection<UserPropertyDto>(); } return _properties; } set { _properties = value; } }
        ICollection<UserPropertyDto> _properties;

        public ICollection<UserClaimDto> Claims { get { if (_claims == null) { _claims = new Collection<UserClaimDto>(); } return _claims; } set { _claims = value; } }
        ICollection<UserClaimDto> _claims;



    }
}
