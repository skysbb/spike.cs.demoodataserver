﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class UserPropertyDto : IHasGuidId, IHasRecordState
    {
        public Guid Id { get; set; }
        public virtual RecordPersistenceState RecordState { get; set; }

        public Guid UserFK { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
