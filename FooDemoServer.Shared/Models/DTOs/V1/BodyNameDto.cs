﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class BodyAliasDto : IHasGuidId, IHasTenantFK, IHasRecordState, IHasDisplayOrderHint
    {
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }
        public Guid BodyFK { get; set; }

        public virtual RecordPersistenceState RecordState { get; set; }
        public int DisplayOrderHint { get; set; }

        public string Name { get; set; }

        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }

    }
}
