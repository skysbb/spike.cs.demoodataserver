﻿using FooDemoServer.Shared.Models.Contracts;
using FooDemoServer.Shared.Models.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace FooDemoServer.Shared.Models.DTOs.V1
{
    public class FooSubItemDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        [Key]
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }


        public virtual RecordPersistenceState RecordState { get; set; }

        public string Text { get; set; }

        public Guid ParentFK { get; set; }

        public virtual FooDto Parent { get { return _foo; } set { _foo = value; } }
        FooDto _foo;
    }

}
